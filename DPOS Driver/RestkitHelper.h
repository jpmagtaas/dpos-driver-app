//
//  RestkitHelper.h
//  DPOS Driver
//
//  Created by Simon Canil on 8/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Order.h"

@interface RestkitHelper : NSObject

- (void) getOrders;
- (void) getAuth :(NSString *)clientId : (NSString * ) driverId : (NSString *) password :(NSString *) reAuth;
- (void) getPostLocationResponse : (Order * ) order eta: (NSNumber *)eta distance: (NSNumber *)distance;
- (void) getPostOrderResponse :(Order * ) order;
- (void) getLastPostOrderResponse :(Order * ) order eta:(NSNumber *)eta;
- (void) getPostResetOrderResponse :(Order * ) order;
- (void) getPostRunResponse :(Order * ) order;

- (void) saveOrders;
- (void) saveAuth: (NSString *) reAuth;

- (void) loadOrders;
- (void) loadAuth;
- (void) clearOrders;
- (void) clearAuth;

- (void) configureRestKit;
- (void) configureRestKitAuth;
- (void) configureRestKitPostLocation;

@property NSMutableArray *orders;
@property NSMutableArray *identities;
@property NSMutableArray *postResponses;

@property (nonatomic, strong) NSArray *parentOrders;
@property (nonatomic, strong) NSArray *identity;

@end
