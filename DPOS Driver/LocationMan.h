//
//  LocationMan.h
//  DPOS Driver
//
//  Created by Simon Canil on 6/10/2015.
//  Copyright © 2015 VisionApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "LocationShareModel.h"

@interface LocationMan : NSObject <CLLocationManagerDelegate>

@property (nonatomic) CLLocationCoordinate2D myLastLocation;
@property (nonatomic) CLLocationAccuracy myLastLocationAccuracy;
@property (strong,nonatomic) LocationShareModel * shareModel;
@property (nonatomic) CLLocationCoordinate2D myLocation;
@property (nonatomic) CLLocationAccuracy myLocationAccuracy;
+ (CLLocationManager *)sharedLocationManager;
+ (LocationMan *)sharedEngine;



@end
