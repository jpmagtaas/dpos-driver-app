//
//  MenuTableViewCell.m
//  DPOS Driver
//
//  Created by Simon Canil on 19/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import "MenuTableViewCell.h"



@implementation MenuTableViewCell


- (void)awakeFromNib {
    // Initialization code
    

    
    // set user details
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
    
    [self.logoutLabel setFont:[UIFont fontWithName:@"fontawesome" size:20]];
    self.logoutLabel.text  = @"\uf011";
    
    [self.iconOrderFeed setFont:[UIFont fontWithName:@"fontawesome" size:20]];
    self.iconOrderFeed.text  = @"\uf03a";
    
    [self.iconDriver setFont:[UIFont fontWithName:@"fontawesome" size:20]];
    self.iconDriver.text  = @"\uf007";
  
    [self.iconStore setFont:[UIFont fontWithName:@"fontawesome" size:20]];
    self.iconStore.text  = @"\uf015";
    
    [self.iconSettings setFont:[UIFont fontWithName:@"fontawesome" size:20]];
    self.iconSettings.text  = @"\uf085";
    
    [self.iconReturn setFont:[UIFont fontWithName:@"fontawesome" size:20]];
    self.iconReturn.text  = @"\uf041";
}

@end
