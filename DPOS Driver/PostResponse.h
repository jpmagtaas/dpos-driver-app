//
//  PostResponse.h
//  DPOS Driver
//
//  Created by Simon Canil on 14/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostResponse : NSObject <NSCoding>

@property (nonatomic, strong) NSString *responseCode;

@end
