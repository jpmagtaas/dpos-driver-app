//
//  OrderHolder.h
//  DPOS Driver
//
//  Created by Simon Canil on 8/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderHolder : NSObject <NSCoding>

@property (strong, nonatomic) NSArray *orders;

@end
