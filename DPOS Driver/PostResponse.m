//
//  PostResponse.m
//  DPOS Driver
//
//  Created by Simon Canil on 14/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import "PostResponse.h"

@implementation PostResponse
#pragma mark -
#pragma mark NSCoding Protocol
- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:self.responseCode forKey:@"responseCode"];
    
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    
    if (self) {
        [self setResponseCode:[decoder decodeObjectForKey:@"responseCode"]];
       
    }
    
    return self;
}
@end
