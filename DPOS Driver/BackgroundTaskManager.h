//
//  BackgroundTaskManager.h
//  DPOS Driver
//
//  Created by Simon Canil on 6/10/2015.
//  Copyright © 2015 VisionApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BackgroundTaskManager : NSObject

+(instancetype)sharedBackgroundTaskManager;

-(UIBackgroundTaskIdentifier)beginNewBackgroundTask;
-(void)endAllBackgroundTasks;

@end
