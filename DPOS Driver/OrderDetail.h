//
//  OrderDetail.h
//  DPOS Driver
//
//  Created by Simon Canil on 8/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Order;

@protocol UpdateOrderDelegate;

@interface OrderDetail : UIViewController <UIAlertViewDelegate>

@property (weak) id<UpdateOrderDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIView *parentView;

@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UIView *amountView;

@property (weak, nonatomic) IBOutlet UIView *notesView;
@property (weak, nonatomic) IBOutlet UILabel *testFontAwesome;

@property (weak, nonatomic) IBOutlet UIView *distanceView;
@property (weak, nonatomic) IBOutlet UIView *timeView;
@property (weak, nonatomic) IBOutlet UILabel *timeToDestLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceToDestinationLabel;

@property (weak, nonatomic) IBOutlet UILabel *custName;
@property (weak, nonatomic) IBOutlet UILabel *amount;
@property (weak, nonatomic) IBOutlet UITextView *orderNotes;
@property (weak, nonatomic) IBOutlet UILabel *timeCount;
@property (weak, nonatomic) IBOutlet UILabel *currentDistance;
@property (weak, nonatomic) IBOutlet UILabel *phone;
@property (weak, nonatomic) IBOutlet UILabel *streetAddress;
@property (weak, nonatomic) IBOutlet UILabel *suburb;
@property (weak, nonatomic) IBOutlet UILabel *upTime;
@property (weak, nonatomic) IBOutlet UILabel *currentTimeToTarget;
@property (weak, nonatomic) IBOutlet UILabel *startTime;
@property (weak, nonatomic) IBOutlet UILabel *stopTime;
@property (weak, nonatomic) IBOutlet UILabel *stopIcon;


@property (weak, nonatomic) IBOutlet UIButton *phoneButton;

@property (weak, nonatomic) IBOutlet UILabel *completeLabel;


@property (weak, nonatomic) IBOutlet UIButton *endButton;
@property (weak, nonatomic) IBOutlet UIButton *startButton;

@property (weak, nonatomic) IBOutlet UIView *paidViewContainer;
@property (weak, nonatomic) IBOutlet UILabel *paidLabel;


@property (weak, nonatomic) IBOutlet UILabel *iconPhone;
@property (weak, nonatomic) IBOutlet UILabel *iconAddress;
@property (weak, nonatomic) IBOutlet UILabel *iconOrderStatus;
@property (weak, nonatomic) IBOutlet UILabel *iconCheck;

@property (weak, nonatomic) IBOutlet UILabel *iconPaid;


@property Order *order;
@end


@protocol UpdateOrderDelegate <NSObject>
- (void)controller:(OrderDetail *)controller didUpdateOrder:(Order *)order;
@end
