//
//  TableViewCellOrders.h
//  DPOS Driver
//
//  Created by Simon Canil on 8/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCellOrders : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *orderNumber;
@property (nonatomic, weak) IBOutlet UILabel *customerName;
@property (nonatomic, weak) IBOutlet UILabel *address;
@property (nonatomic, weak) IBOutlet UILabel *orderAge;
@property (nonatomic, weak) IBOutlet UILabel *distance;
@property (nonatomic, weak) IBOutlet UILabel *status;
@property (weak, nonatomic) IBOutlet UILabel *iconOrder;
@property (weak, nonatomic) IBOutlet UILabel *iconOverdue;

@end
