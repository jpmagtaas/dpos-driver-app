//
//  RestkitHelper.m
//  DPOS Driver
//
//  Created by Simon Canil on 8/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import "RestkitHelper.h"
#import <RestKit/RestKit.h>
#import "OrderHolder.h"
#import "Order.h"
#import "Identity.h"
#import "PostResponse.h"
#import "KeychainItemWrapper.h"

@implementation RestkitHelper

- (void)loadOrders
{
    
    
    
    NSString *filePath;
    
    filePath = [self pathForOrders];
 
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        self.orders = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
        
    } else {
        self.orders = [NSMutableArray array];
    }
    //adding a comment  

    
}

-(void) clearOrders {
    NSString *filePath;
    
    filePath = [self pathForOrders];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        
        NSError *error = nil;
        
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
        
    }
}

- (void)loadAuth
{
    
    
    
    NSString *filePath;
    
    filePath = [self pathForAuth];
    
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        self.identities = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
        
    } else {
        self.identities = [NSMutableArray array];
    }
    
    
    
}

-(void) clearAuth {
    NSString *filePath;
    
    filePath = [self pathForAuth];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        
        NSError *error = nil;
        
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
        
    }
}



// Main helper functions
- (void)configureRestKit
{
    // initialize AFNetworking HTTPClient
    NSURL *baseURL = [NSURL URLWithString:@"https://api.dsoftonline.com.au/driver/"];
    
    //THIS IS FOR TEST ENVIRONMENT
    NSURL *baseStagingURL = [NSURL URLWithString:@"https://api.dsoftonline.com.au/driver-staging/"];
    
    //THIS IS FOR LOCAL TEST ENVIRONMENT
    NSURL *baseLocalURL = [NSURL URLWithString:@"http://192.168.1.37/dpos/mobile.driverAPI/driver/"];
    
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    
    // initialize RestKit
    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:client];
    
    [objectManager setRequestSerializationMIMEType:RKMIMETypeJSON];
    [objectManager setAcceptHeaderWithMIMEType:RKMIMETypeJSON];
    [objectManager addResponseDescriptorsFromArray:[RKObjectManager sharedManager].responseDescriptors];
    [objectManager addRequestDescriptorsFromArray:[RKObjectManager sharedManager].requestDescriptors];
    [RKObjectManager setSharedManager:objectManager];
    
    
    // setup object mappings
    RKObjectMapping *orderMapping = [RKObjectMapping mappingForClass:[Order class]];
    [orderMapping addAttributeMappingsFromDictionary:@{@"order_id": @"orderId",
                                                       @"run_id": @"runId",
                                                       @"in_time": @"inTime",
                                                       @"out_time": @"outTime",
                                                       @"StartTime": @"startTime",
                                                       @"EndTime": @"endTime",
                                                       @"quoted_time": @"quotedTime",
                                                       @"status": @"status",
                                                       @"order_notes": @"orderNotes",
                                                       @"tendered_amount": @"tenderAmount",
                                                       @"paid_amount": @"paidAmount",
                                                       @"customer_name": @"customerName",
                                                       @"customer_phone": @"customerPhone",
                                                       @"address": @"streetName",
                                                       @"Suburb": @"suburb",
                                                       @"longitude": @"lon",
                                                       @"latitude": @"lat",
                                                       @"response_code": @"responseCode"}];
    // register mappings with the provider using a response descriptor
    RKResponseDescriptor *responseDescriptor1 =
    [RKResponseDescriptor responseDescriptorWithMapping:orderMapping
                                                 method:RKRequestMethodGET
                                            pathPattern:@"deliveries.php"
                                                keyPath:@"deliveries"
                                            statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [objectManager addResponseDescriptor:responseDescriptor1];
    
    
    
    // setup object mappings
    RKObjectMapping *authMapping = [RKObjectMapping mappingForClass:[Identity class]];
    [authMapping addAttributeMappingsFromDictionary:@{@"store_name": @"storeName",
                                                      @"longitude": @"lon",
                                                      @"latitude": @"lat",
                                                      @"driver_name": @"driverName",
                                                      @"API_token": @"apiToken",
                                                      @"response_code": @"responseCode"}];
    
    // register mappings with the provider using a response descriptor
    RKResponseDescriptor *responseDescriptorAuth =
    [RKResponseDescriptor responseDescriptorWithMapping:authMapping
                                                 method:RKRequestMethodGET
                                            pathPattern:@"get_key.php"
                                                keyPath:@""
                                            statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [objectManager addResponseDescriptor:responseDescriptorAuth];
    
    
    // setup object mappings location post
    RKObjectMapping *postLocationMapping = [RKObjectMapping mappingForClass:[PostResponse class]];
    [postLocationMapping addAttributeMappingsFromDictionary:@{@"response_code": @"responseCode"}];
    
    // register mappings with the provider using a response descriptor
    RKResponseDescriptor *responseDescriptor =
    [RKResponseDescriptor responseDescriptorWithMapping:postLocationMapping
                                                 method:RKRequestMethodGET
                                            pathPattern:@"update_driver_location.php"
                                                keyPath:@""
                                            statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [objectManager addResponseDescriptor:responseDescriptor];
    
    
    // setup object mappings update order post
    RKObjectMapping *postUpdateOrderMapping = [RKObjectMapping mappingForClass:[PostResponse class]];
    [postUpdateOrderMapping addAttributeMappingsFromDictionary:@{@"response_code": @"responseCode"}];
    
    // register mappings with the provider using a response descriptor
    RKResponseDescriptor *responseDescriptorUpdateOrder =
    [RKResponseDescriptor responseDescriptorWithMapping:postUpdateOrderMapping
                                                 method:RKRequestMethodGET
                                            pathPattern:@"update_delivery.php"
                                                keyPath:@""
                                            statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [objectManager addResponseDescriptor:responseDescriptorUpdateOrder];
    
    
    
    // setup object mappings refresh order post
    RKObjectMapping *postRefreshOrderMapping = [RKObjectMapping mappingForClass:[PostResponse class]];
    [postRefreshOrderMapping addAttributeMappingsFromDictionary:@{@"response_code": @"responseCode"}];
    
    // register mappings with the provider using a response descriptor
    RKResponseDescriptor *responseDescriptorRefreshOrder =
    [RKResponseDescriptor responseDescriptorWithMapping:postRefreshOrderMapping
                                                 method:RKRequestMethodGET
                                            pathPattern:@"reset_order.php"
                                                keyPath:@""
                                            statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [objectManager addResponseDescriptor:responseDescriptorRefreshOrder];
    

    
    // setup object mappings update run post
    RKObjectMapping *postRunMapping = [RKObjectMapping mappingForClass:[PostResponse class]];
    [postRunMapping addAttributeMappingsFromDictionary:@{@"response_code": @"responseCode"}];
    
    // register mappings with the provider using a response descriptor
    RKResponseDescriptor *responseDescriptorPostRun =
    [RKResponseDescriptor responseDescriptorWithMapping:postRunMapping
                                                 method:RKRequestMethodGET
                                            pathPattern:@"end_run.php"
                                                keyPath:@""
                                            statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [objectManager addResponseDescriptor:responseDescriptorPostRun];
   
    
    
    
    
}


- (void)configureRestKitAuth
{
    // initialize AFNetworking HTTPClient
    NSURL *baseURL = [NSURL URLWithString:@"https://api.dsoftonline.com.au"];
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    
    // initialize RestKit
    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:client];
    
    [objectManager setRequestSerializationMIMEType:RKMIMETypeJSON];
    [objectManager setAcceptHeaderWithMIMEType:RKMIMETypeJSON];
    [objectManager addResponseDescriptorsFromArray:[RKObjectManager sharedManager].responseDescriptors];
    [objectManager addRequestDescriptorsFromArray:[RKObjectManager sharedManager].requestDescriptors];
    [RKObjectManager setSharedManager:objectManager];
    
    
    // setup object mappings
    RKObjectMapping *authMapping = [RKObjectMapping mappingForClass:[Identity class]];
    [authMapping addAttributeMappingsFromDictionary:@{@"store_name": @"storeName",
                                                       @"longitude": @"lon",
                                                       @"latitude": @"lat",
                                                       @"driver_name": @"driverName",
                                                      @"API_token": @"apiToken",
                                                      @"response_code": @"responseCode"}];
    
    // register mappings with the provider using a response descriptor
    RKResponseDescriptor *responseDescriptor =
    [RKResponseDescriptor responseDescriptorWithMapping:authMapping
                                                 method:RKRequestMethodGET
                                            pathPattern:@"get_key.php"
                                                keyPath:@""
                                            statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [objectManager addResponseDescriptor:responseDescriptor];
    
    
}


- (void)configureRestKitPostLocation
{
    
    // initialize AFNetworking HTTPClient
    NSURL *baseURL = [NSURL URLWithString:@"https://api.dsoftonline.com.au"];
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    // initialize RestKit
    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:client];
    
    [objectManager setRequestSerializationMIMEType:RKMIMETypeJSON];
    [objectManager setAcceptHeaderWithMIMEType:RKMIMETypeJSON];
    [objectManager addResponseDescriptorsFromArray:[RKObjectManager sharedManager].responseDescriptors];
    [objectManager addRequestDescriptorsFromArray:[RKObjectManager sharedManager].requestDescriptors];
    [RKObjectManager setSharedManager:objectManager];

}

-(void)getPostLocationResponse:(Order *)order eta:(NSNumber *)eta distance:(NSNumber *)distance {
    NSLog(@"In load.");
    
    [self loadAuth];
    Identity *identity = _identities[0];
    
    NSLog(@"ORDER NYA TO %@" , order.orderId);
    NSLog(@"lat: %@", order.lat);
    NSLog(@"lon: %@", order.lon);
    NSLog(@"API TOKEN: %@", identity.apiToken);
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
    
    NSMutableDictionary *mutableParams = [[NSMutableDictionary alloc] init];
    
    [mutableParams setObject:identity.apiToken forKey:@"API_token"];
    [mutableParams setObject:order.lat forKey:@"latitude"];
    [mutableParams setObject:order.lon forKey:@"longitude"];
    [mutableParams setObject:[dateFormatter stringFromDate:[NSDate date]] forKey:@"timestamp"];
    //[mutableParams setObject:eta forKey:@"eta"];
    //[mutableParams setObject:distance forKey:@"distance"];
    
    /*NSDictionary *queryParams = @{@"API_token" : identity.apiToken,
                                  @"latitude" : order.lat,
                                  @"longitude" : order.lon,
                                  @"timestamp" : [dateFormatter stringFromDate:[NSDate date]]};*/
    
    NSLog(@"Calling post service:");
    
    [[RKObjectManager sharedManager] getObjectsAtPath:@"update_driver_location.php"
                                           parameters:mutableParams
                                              success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                  
                                                  NSLog(@"**************** POSTED LOCATION *************");
                                                  
                                                  
                                                  self.postResponses = [mappingResult.array mutableCopy];
                                                  PostResponse *pr = _postResponses[0];
                                                  
                                                  NSLog(@"Response from post: %@", pr.responseCode);
                                                  
                                                  
                                                  if (([pr.responseCode isEqualToString:@"01"]) ||
                                                      ([pr.responseCode isEqualToString:@"02"])){
                                                      
                                                      KeychainItemWrapper *keychain =
                                                      [[KeychainItemWrapper alloc] initWithIdentifier:@"VisionKeyChain" accessGroup:nil];
                                                      // get keychain items and call auth
                                                      NSString *clientId = [keychain objectForKey:(__bridge id)kSecAttrDescription];
                                                      NSString *userId = [keychain objectForKey:(__bridge id)kSecAttrAccount];
                                                      NSString *password = [keychain objectForKey:(__bridge id)kSecValueData];
                                                      
                                                      NSLog(@">>> Calling AUTH Retry");
                                                      [self getAuthRetry:order:clientId :userId :password :@"Y": @"LOCATION_POST"];
                                                      // get
                                                      
                                                  }else{
                                                                                                       }
                                                  NSLog(@">>> Do nothing - assume success");
                                                  
                                              }
                                              failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                  NSLog(@"Having trouble with post location hey?': %@", error);
                                              }];
    NSLog(@"Here");
}

- (void)getPostOrderResponse :(Order * ) order
{
    
    NSLog(@"In load.");
    
    [self loadAuth];
    Identity *identity = _identities[0];
    
    NSLog(@"DarioId: %@", order.orderId);
    NSLog(@"DarioPhone %@", order.customerPhone);
    
    NSLog(@"API TOKEN: %@", identity.apiToken);
    
    // get system time.
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-mm-dd HH:mm:ss"];
    NSString *resultString = [dateFormatter stringFromDate: currentTime];

    NSLog(@"Update time param: %@", resultString);
    
    NSString *phoneNumber = [[NSString alloc] init];
    
    if(order.customerPhone == NULL || [order.customerPhone isKindOfClass:[NSNull class]]) {
        phoneNumber = @"";
    } else {
        phoneNumber = order.customerPhone;
    }
    
    NSDictionary *queryParams = @{@"API_token" : identity.apiToken,
                                  @"Order_id" : order.orderId,
                                  @"time" : resultString,
                                  @"phone_number" : phoneNumber
                
                                  };
    
    [self getPostOrderService:queryParams order:order];
    
    NSLog(@"Here");
}

-(void)getLastPostOrderResponse:(Order *)order eta:(NSNumber *)eta {
    NSLog(@"In load.");
    
    [self loadAuth];
    Identity *identity = _identities[0];
    
    // get system time.
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-mm-dd HH:mm:ss"];
    NSString *resultString = [dateFormatter stringFromDate: currentTime];

    NSLog(@"Update time param: %@", resultString);
    
    NSString *phoneNumber = [[NSString alloc] init];
    
    if(order.customerPhone == NULL || [order.customerPhone isKindOfClass:[NSNull class]]) {
        phoneNumber = @"";
    } else {
        phoneNumber = order.customerPhone;
    }
    
    NSDictionary *queryParams = @{@"API_token" : identity.apiToken,
                                  @"Order_id" : order.orderId,
                                  @"time" : resultString,
                                  @"phone_number" : phoneNumber,
                                  @"eta" : eta
                                  };
    
    [self getPostOrderService:queryParams order:order];
}
    
-(void) getPostOrderService:(NSDictionary *)queryParams order: (Order *)order {
    
    [[RKObjectManager sharedManager] getObjectsAtPath:@"update_delivery.php"
    parameters:queryParams
       success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
           
           NSLog(@"**************** POSTED ORDER UPDATE *************");
           
           
           self.postResponses = [mappingResult.array mutableCopy];
           PostResponse *pr = _postResponses[0];
           
           NSLog(@"Response from post: %@", pr.responseCode);
           
           
           if (([pr.responseCode isEqualToString:@"01"]) ||
               ([pr.responseCode isEqualToString:@"02"])){
           
               KeychainItemWrapper *keychain =
               [[KeychainItemWrapper alloc] initWithIdentifier:@"VisionKeyChain" accessGroup:nil];
               // get keychain items and call auth
               NSString *clientId = [keychain objectForKey:(__bridge id)kSecAttrDescription];
               NSString *userId = [keychain objectForKey:(__bridge id)kSecAttrAccount];
               NSString *password = [keychain objectForKey:(__bridge id)kSecValueData];

               NSLog(@">>> Calling AUTH Retry");
               [self getAuthRetry:order:clientId :userId :password :@"Y": @"ORDER_UPDATE"];
               // get
               
           }else{
               
           [[NSNotificationCenter defaultCenter] postNotificationName:@"PostOrderSuccessNotification"
                                                               object:nil
                                                             userInfo:nil];
           }
         }
       failure:^(RKObjectRequestOperation *operation, NSError *error) {
           NSLog(@"Having trouble with post order update hey?': %@", error);
       }];
    
}


- (void)getPostResetOrderResponse :(Order * ) order
{
    NSLog(@"In Refresh Order.");
    
    [self loadAuth];
    Identity *identity = _identities[0];
    
    NSLog(@"orderId: %@ and run Id: %@", order.orderId, order.runId);
    
    NSLog(@"API TOKEN: %@", identity.apiToken);
    
    // get system time.
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-mm-dd HH:mm:ss"];
    NSString *resultString = [dateFormatter stringFromDate: currentTime];
    
    NSLog(@"Update time param: %@", resultString);
    
    NSDictionary *queryParams = @{@"API_token" : identity.apiToken,
                                  @"OrderId" : order.orderId,
                                  @"RunId" : order.runId};
    
    [[RKObjectManager sharedManager] getObjectsAtPath:@"reset_order.php"
                                           parameters:queryParams
                                              success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                  
                                                  NSLog(@"**************** POSTED ORDER UPDATE *************");
                                                 
                                                  
                                                  self.postResponses = [mappingResult.array mutableCopy];
                                                  PostResponse *pr = _postResponses[0];
                                                  
                                                  NSLog(@"Response from post: %@", pr.responseCode);
                                                  
                                                  
                                                  if (([pr.responseCode isEqualToString:@"01"]) ||
                                                      ([pr.responseCode isEqualToString:@"02"])){
                                                      
                                                      KeychainItemWrapper *keychain =
                                                      [[KeychainItemWrapper alloc] initWithIdentifier:@"VisionKeyChain" accessGroup:nil];
                                                      // get keychain items and call auth
                                                      NSString *clientId = [keychain objectForKey:(__bridge id)kSecAttrDescription];
                                                      NSString *userId = [keychain objectForKey:(__bridge id)kSecAttrAccount];
                                                      NSString *password = [keychain objectForKey:(__bridge id)kSecValueData];
                                                      
                                                      NSLog(@">>> Calling AUTH Retry");
                                                      [self getAuthRetry:order:clientId :userId :password :@"Y": @"ORDER_RESET"];
                                                      // get
                                                      
                                                  }else{
                                                      
                                                      [[NSNotificationCenter defaultCenter] postNotificationName:@"PostOrderResetNotification"
                                                                                                          object:nil
                                                                                                        userInfo:nil];
                                                  }

                                              }
                                              failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                  NSLog(@"Having trouble with post order update hey?': %@", error);
                                              }];
    NSLog(@"Here");

}



- (void)getPostRunResponse :(Order * ) order
{
    
    NSLog(@"In END RUN.");
    
    [self loadAuth];
    Identity *identity = _identities[0];
    
    
    NSLog(@"API TOKEN: %@", identity.apiToken);
    
    // get system time.
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-mm-dd HH:mm:ss"];
    NSString *resultString = [dateFormatter stringFromDate: currentTime];
    
    NSLog(@"Update time param: %@", resultString);
    
     NSLog(@"Run Id: %@", order.runId);
    
    NSDictionary *queryParams = @{@"API_token" : identity.apiToken,
                                  @"Run_id" : order.runId,
                                  @"time" : resultString};
    
    [[RKObjectManager sharedManager] getObjectsAtPath:@"end_run.php"
                                           parameters:queryParams
                                              success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                  
                                                  NSLog(@"**************** POSTED RUN END *************");
                                                  
                                                  
                                                  
                                                  self.postResponses = [mappingResult.array mutableCopy];
                                                  PostResponse *pr = _postResponses[0];
                                                  
                                                  NSLog(@"Response from post: %@", pr.responseCode);
                                                  
                                                  
                                                  if (([pr.responseCode isEqualToString:@"01"]) ||
                                                      ([pr.responseCode isEqualToString:@"02"])){
                                                      
                                                      KeychainItemWrapper *keychain =
                                                      [[KeychainItemWrapper alloc] initWithIdentifier:@"VisionKeyChain" accessGroup:nil];
                                                      // get keychain items and call auth
                                                      NSString *clientId = [keychain objectForKey:(__bridge id)kSecAttrDescription];
                                                      NSString *userId = [keychain objectForKey:(__bridge id)kSecAttrAccount];
                                                      NSString *password = [keychain objectForKey:(__bridge id)kSecValueData];
                                                      
                                                      NSLog(@">>> Calling AUTH Retry");
                                                      [self getAuthRetry:order:clientId :userId :password :@"Y": @"RUN_END"];
                                                      // get
                                                      
                                                  }else{
                                                      
                                                      [[NSNotificationCenter defaultCenter] postNotificationName:@"RunEndedNotification"
                                                                                                          object:nil
                                                                                                        userInfo:nil];
                                                  }
                                              }
                                              failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                  NSLog(@"Having trouble with post run ende hey?': %@", error);
                                              }];
    NSLog(@"Here");
}

- (void)getAuth :(NSString *)clientId : (NSString * ) driverId : (NSString *) password :(NSString *) reAuth
{
    
    NSLog(@"In load.");
    NSLog(@"StoreID: %@", clientId);
    NSLog(@"driverId: %@", driverId);
    NSLog(@"password: %@", password);
    
    NSDictionary *queryParams = @{@"ClientId" : clientId,
                                  @"DriverId" : driverId,
                                  @"password" : password};
    
    
    
    
    NSLog(@"Calling service: ");
    
    [[RKObjectManager sharedManager] getObjectsAtPath:@"get_key.php"
                                           parameters:queryParams
                                              success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                  
                                                  NSLog(@"Success");
                                                  
                                                  _identity = mappingResult.array;
                                                  
                                                  self.identities = [mappingResult.array mutableCopy];
                                                  
                                                  NSLog(@"Copied resluts %@", mappingResult);
                                                  
                                                  Identity *identity = _identities[0];
                                                  
                                                  NSLog(@"First AUTH: %@", identity.driverName);
                                                  NSLog(@"API TOKEN: %@", identity.apiToken);
                                                  NSLog(@"RESPONSE CODE: %@", identity.responseCode);
                                                  if ([identity.responseCode isEqualToString:@"03"]){
                                                      [[NSNotificationCenter defaultCenter] postNotificationName:@"AuthFailedNotification" object:self];
                                                  }else{
                                                      
                                                      [self saveAuth:reAuth];
                                                  }
                                                  
                                                  
                                              }
                                              failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                  NSLog(@"Having trouble with order mapping hey?': %@", error);
                                              }];
    NSLog(@"Here");
}


- (void)getAuthRetry :(Order *) order :(NSString *)clientId : (NSString * ) driverId : (NSString *) password :(NSString *) reAuth : (NSString *) callbackService
{
    
    NSLog(@"In Auth RETRY.");
    NSLog(@"StoreID: %@", clientId);
    NSLog(@"driverId: %@", driverId);
    NSLog(@"password: %@", password);
    
    NSDictionary *queryParams = @{@"ClientId" : clientId,
                                  @"DriverId" : driverId,
                                  @"password" : password};
    
    
    
    
    
    [[RKObjectManager sharedManager] getObjectsAtPath:@"get_key.php"
                                           parameters:queryParams
                                              success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                  
                                                  NSLog(@"Success");
                                                  
                                                  _identity = mappingResult.array;
                                                  
                                                  self.identities = [mappingResult.array mutableCopy];
                                                  
                                                  NSLog(@"Copied resluts %@", mappingResult);
                                                  
                                                  Identity *identity = _identities[0];
                                                  
                                                  NSLog(@"First AUTH: %@", identity.driverName);
                                                  NSLog(@"API TOKEN: %@", identity.apiToken);
                                                  NSLog(@"RESPONSE CODE: %@", identity.responseCode);
                                                  if ([identity.responseCode isEqualToString:@"03"]){
                                                      [[NSNotificationCenter defaultCenter] postNotificationName:@"AuthFailedNotification" object:self];
                                                  }else{
                                                      
                                                      [self saveAuth:reAuth];
                                                      
                                                      if ([callbackService isEqualToString:@"ORDERS"]){
                                                          
                                                          NSLog(@"Retry get orders");
                                                          [self getOrders];
                                                      }else if ([callbackService isEqualToString:@"ORDER_UPDATE"]){
                                                          NSLog(@"Retry post order update");
                                                          [self getPostOrderResponse:order];
                                                      }else if ([callbackService isEqualToString:@"RUN_END"]){
                                                          NSLog(@"Retry post order update");
                                                          [self getPostRunResponse:order];
                                                      }else if ([callbackService isEqualToString:@"ORDER_RESET"]){
                                                          NSLog(@"Retry post order update");
                                                          [self getPostRunResponse:order];
                                                      }else if ([callbackService isEqualToString:@"LOCATION_POST"]){
                                                          NSLog(@"Retry post order update");
                                                          //[self getPostLocationResponse:order];
                                                          [self getPostLocationResponse:order eta:@0 distance:@0];
                                                      }
                                                  }
                                                  
                                                  
                                              }
                                              failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                  NSLog(@"Having trouble with order mapping hey?': %@", error);
                                              }];
    NSLog(@"Here");
}


- (void)getOrders
{
    
    
    [self loadAuth];
    
    NSLog(@"In load.");
    Identity *identity = _identities[0];
    
    NSLog(@"First AUTH: %@", identity.driverName);
    NSLog(@"API TOKEN: %@", identity.apiToken);
    
    NSString *apiKey = identity.apiToken;
    
    NSDictionary *queryParams = @{@"API_token" : apiKey};
    
    
    NSLog(@"Calling service Orders: ");
    
    [[RKObjectManager sharedManager] getObjectsAtPath:@"deliveries.php"
                                           parameters:queryParams
                                              success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                  
                                                  NSLog(@"Success");
                                                  
                                                  _parentOrders = mappingResult.array;
                                                  
                                                  self.orders = [mappingResult.array mutableCopy];
                                                                                                    
                                                  NSLog(@"Copied resluts %@", mappingResult);
                                                 
                                                  if(_parentOrders.count > 0){
                                                      Order *order = _parentOrders[0];
                                                      [self saveOrders];
                                                  
                                                      NSLog(@"order response: %@", order.responseCode);
                                                      if (([order.responseCode isEqualToString:@"01"]) ||
                                                                                ([order.responseCode isEqualToString:@"02"])){
                                                          
                                                          KeychainItemWrapper *keychain =
                                                          [[KeychainItemWrapper alloc] initWithIdentifier:@"VisionKeyChain" accessGroup:nil];
                                                          // get keychain items and call auth
                                                          NSString *clientId = [keychain objectForKey:(__bridge id)kSecAttrDescription];
                                                          NSString *userId = [keychain objectForKey:(__bridge id)kSecAttrAccount];
                                                          NSString *password = [keychain objectForKey:(__bridge id)kSecValueData];
                                                      
                                                          
                                                          Order *o = [Order new];
                                                          
                                                          [self getAuthRetry:o:clientId :userId :password :@"Y": @"ORDERS"];
                                                        }else{
                                                            
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"RestSuccessNotification"
                                                                                                     object:nil
                                                                                                     userInfo:nil];
                                                    
                                                
                                                        }
                                                  
                                                  }else{
                                                  
                                                  [[NSNotificationCenter defaultCenter] postNotificationName:@"RestSuccessNotification"
                                                                                                      object:nil
                                                                                                    userInfo:nil];
                                                  
                                                  NSLog(@"@@@@@@@@@@@@ 0 orders @@@@@@@@@@@@");
                                                      
                                                  }
                                                  
                                              }
                                              failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                  NSLog(@"Having trouble with order mapping hey?': %@", error);
                                              }];
    

    NSLog(@"Here");
}

- (void)saveOrders{
    NSString *filePath = [self pathForOrders];
    [NSKeyedArchiver archiveRootObject:self.parentOrders toFile:filePath];
    // Post Notification
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"TSPShoppingListDidChangeNotification" object:self];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OrdersSavedNotification" object:self];
    
    NSLog(@"orders saved to : %@", filePath);
}


- (NSString *)pathForOrders{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documents = [paths lastObject];
    
    NSString *returnStr = @"";
    
    returnStr = [documents stringByAppendingPathComponent:@"driverStoreOrders.plist"];
    
    return returnStr;
}

- (void)saveAuth: (NSString *) reAuth{
    NSString *filePath = [self pathForAuth];
    [NSKeyedArchiver archiveRootObject:self.identities toFile:filePath];
    // Post Notification
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"TSPShoppingListDidChangeNotification" object:self];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AuthSavedNotification" object:self];
    
    NSLog(@"auth saved to : %@", filePath);
}


- (NSString *)pathForAuth{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documents = [paths lastObject];
    
    NSString *returnStr = @"";
    
    returnStr = [documents stringByAppendingPathComponent:@"auth.plist"];
    
    return returnStr;
}


@end
