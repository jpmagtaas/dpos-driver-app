//
//  OrderDetail.m
//  DPOS Driver
//
//  Created by Simon Canil on 8/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import "OrderDetail.h"
#import "Order.h"
#import "Utils.h"
#import "RestkitHelper.h"
#import <MapKit/MapKit.h>
#import "OrderWatcher.h"

@interface OrderDetail ()

@end

@implementation OrderDetail
NSTimer *myTicker;
int count = 0;
CAGradientLayer *layerDefaultBg;
CAGradientLayer *layerStartedBg;
CAGradientLayer *layerCompletedBg;

- (void)viewDidLoad {
   
    [super viewDidLoad];
    
    [self configureLayout];
    
    [self setButtonVisibility];
    
    [self estimateArrivals];
    
    
    NSLog(@">>>>>>>>>>Order RuN: %@", self.order.runId);
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) showMapSegue{
    
    NSLog(@"ShowMap");
    [self performSegueWithIdentifier:@"map" sender:self];

}

- (IBAction)phoneClicked:(id)sender {
    
    NSString *phoneNumber = [@"tel://" stringByAppendingString:self.order.customerPhone];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}


- (void) configureLayout{
    

    
    // add button for map
    UIBarButtonItem *mapButton = [[UIBarButtonItem alloc] initWithTitle:@"Map" style:UIBarButtonItemStylePlain target:self action:@selector(showMapSegue)];
    self.navigationItem.rightBarButtonItem = mapButton;

    //object for order has been loaded by parent view into self.order property we can access this straight away.
    //set title
    self.title = [@"#" stringByAppendingString:self.order.orderId];
    
    // set labels
    [self.custName setText:[self.order customerName]];
    [self.orderNotes setText:[self.order orderNotes]];
    [self.phone setText:[self.order customerPhone]];
    [self.streetAddress setText:[self.order streetName]];
    [self.suburb setText:[self.order suburb]];
    
    
    // [self.iconPaid setText:[self.order paidAmount]];
    self.iconPaid.hidden = YES;
    
    // set button label for phone,
    [self.phoneButton setTitle:self.order.customerPhone forState:UIControlStateNormal];
    
    
    //set font awesome icons.
    [self.iconPhone setFont:[UIFont fontWithName:@"fontawesome" size:20]];
    self.iconPhone.text = @"\uf095";
    [self.iconAddress setFont:[UIFont fontWithName:@"fontawesome" size:20]];
    self.iconAddress.text = @"\uf015";
    [self.iconOrderStatus setFont:[UIFont fontWithName:@"fontawesome" size:20]];
    self.iconOrderStatus.text = @"\uf017";
    
    
    
    // apply cel borders
    self.headerView.layer.borderColor = [[UIColor grayColor] CGColor];
    self.amountView.layer.borderColor = [[UIColor grayColor] CGColor];
    self.notesView.layer.borderColor = [[UIColor grayColor] CGColor];
    self.distanceView.layer.borderColor = [[UIColor grayColor] CGColor];
    self.timeView.layer.borderColor = [[UIColor grayColor] CGColor];
    
    
    //price formatting
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *price = [f numberFromString:[self.order tenderAmount]];
    
    NSNumber *paid = [f numberFromString:[self.order paidAmount]];
    
    NSNumber *due =  [NSNumber numberWithFloat:([price floatValue] - [paid floatValue])];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSString *formattedAmount = [formatter stringFromNumber:due];
    
    
    NSString *amountLabel = [@"Total Due: " stringByAppendingString:formattedAmount];
    [self.amount setText:amountLabel];
    
    // start and stop times
    
    UIColor *greenColor =
    [UIColor colorWithRed:0.34 green:0.72 blue:0.17 alpha:1.0];
    
    self.stopTime.textColor = greenColor;
    
    if ([self.order startTime] != nil){
        
        NSLog(@"Start time: %@", [self.order startTime]);
        self.startTime.text = [Utils formatDateStringToTimeString:[self.order startTime]];
        
        UIColor *greenColor =
        [UIColor colorWithRed:255.0f/255.0f green:149.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
        
        self.startTime.textColor = greenColor;

    }else{
        
        self.startTime.text = @"Not Started";
        
    }
    
    if ([self.order endTime] != nil){
        
        self.stopTime.text = [Utils formatDateStringToTimeString:[self.order endTime]];
        [self.stopIcon setFont:[UIFont fontWithName:@"fontawesome" size:20]];
         self.stopIcon.text = @"\uf05d";
        
        }else{
        
        self.stopTime.text = @"";
        self.stopIcon.hidden = YES;
    }
    
   
    
    //header view background:
    [self configureBg];
    
    
    
    //paid borders
    
    self.paidViewContainer.layer.cornerRadius = 8;
    self.paidViewContainer.layer.borderWidth = 3;
    self.paidViewContainer.layer.borderColor = [[UIColor colorWithRed:0.34 green:0.72 blue:0.17 alpha:1.0] CGColor];
    self.paidLabel.textColor = [UIColor colorWithRed:0.34 green:0.72 blue:0.17 alpha:1.0];
    
    //paid visibility TODO: partial paid.
    
    
    if (price == paid){
        self.paidLabel.text = @"PAID";
        self.paidViewContainer.hidden = NO;
        
    }else{
        self.paidLabel.text = @"";
        self.paidViewContainer.hidden = YES;
    }
    
    
}


- (void) setButtonVisibility{
    
    [self.iconCheck setFont:[UIFont fontWithName:@"fontawesome" size:36]];
    
    if ([self.order.status isEqualToString:@"STARTED"]){
        [self.startButton setHidden:YES];
        [self.endButton setHidden:NO];
        [self.completeLabel setHidden:YES];
        
        self.iconCheck.text = @"\uf1b9";
        
    }else if ([self.order.status isEqualToString:@"COMPLETED"]){
        [self.startButton setHidden:YES];
        [self.endButton setHidden:YES];
        [self.completeLabel setHidden:NO];
        
        self.iconCheck.text = @"\uf00c";
    }
    else{
        
        [self.startButton setHidden:NO];
        [self.endButton setHidden:YES];
        [self.completeLabel setHidden:YES];
        
        self.iconCheck.text = @"\uf04b";
    }
    
}


-(void)configureBg {
    
    if ([self.order.status isEqualToString:@"STARTED"]){
        
        [self configureStartedBg];
        
    }else if ([self.order.status isEqualToString:@"COMPLETED"]){
        
        [self configureCompletedBg];
    }
    else{
        
        [self configureDefaultBg];
    }
    
    
}

-(void) estimateArrivals {
    CLLocationCoordinate2D location;
    
    location.latitude = [self.order.lat doubleValue];
    location.longitude = [self.order.lon doubleValue];
    
    MKPlacemark* placemark = [[MKPlacemark alloc] initWithCoordinate:location addressDictionary:nil];
    
    MKMapItem* destinationMapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    
    /** TODO ADD OPTION DETAILS */
    //NSDictionary* mapLaunchOptions;
    // [destinationMapItem openInMapsWithLaunchOptions:mapLaunchOptions];
    
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    [request setSource:[MKMapItem mapItemForCurrentLocation]];
    [request setDestination:destinationMapItem];
    [request setTransportType:MKDirectionsTransportTypeAutomobile]; // This can be limited to automobile and walking directions.
    [request setRequestsAlternateRoutes:NO]; // Gives you several route options.
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
   /**
    [directions calculateETAWithCompletionHandler:^(MKETAResponse *response, NSError *error)
     {
         NSString *eta = [Utils stringFromTimeInterval:response.expectedTravelTime];
         NSString *distance = [Utils stringFromTimeInterval:response.];
         
         NSLog(@"ETA route1: %@", eta);
         
         self.timeToDestLabel.text = eta;
         
         
     }];
    */
     [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
     if (!error) {
         MKRoute *route1 = [[response routes] objectAtIndex:0];
     
         NSString *eta = [Utils stringFromTimeInterval:route1.expectedTravelTime];
         // NSString *distance = [NSString stringWithFormat:@"%.1f meters",route1.distance];
         
         double km = route1.distance / 1000;
         
     
         NSLog(@"ETA route1: %@", eta);
         self.timeToDestLabel.text = eta;
         self.distanceToDestinationLabel.text = [NSString stringWithFormat:@"%.1f km",km];;
         
         for (MKRoute *route in [response routes]) {
     
     
             for (MKRouteStep *step in route.steps)
             {
     
               //  NSLog(@"%@", step.instructions);
             }
     
         }
        }
     }];
    
}

-(void)completeOrderFromAlert {

    [myTicker invalidate];
    
    [self stopSpin];
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *resultString = [dateFormatter stringFromDate: currentTime];
    NSLog(@"Stopping Order %@", currentTime);
    
    // update status and time recieved.
    self.order.status = @"COMPLETED";
    self.order.endTime = resultString;
    
    //configure stop time to show.
    self.stopTime.text = [Utils formatDateStringToTimeString:resultString];
    [self.stopIcon setFont:[UIFont fontWithName:@"fontawesome" size:20]];
    self.stopIcon.text = @"\uf05d";
    self.stopIcon.hidden = NO;
    
    //wrap order for location notification:
    
    NSDictionary* dict = [NSDictionary dictionaryWithObject:self.order
                                                     forKey:@"order"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderStopeedNotification" object:self.order userInfo:dict];
    
    // notify delegate of update.
    [self setButtonVisibility];
    [self.delegate controller:self didUpdateOrder:self.order];
    NSLog(@"Delegate Called");
    [self configureBg];
    
    [[OrderWatcher sharedEngine] stopOrder:self.order];
    
    
    NSLog(@"Watcher orderInProgress property set to: %d", [OrderWatcher sharedEngine].orderInProgress);
    
    [self.navigationController popToRootViewControllerAnimated:YES];

}

-(void)startorderFromAlert{
    
    // check to see if existing order in progress
    
    if ([OrderWatcher sharedEngine].isOrderInProgress == YES){
        
        [self alertOrderInProgress];
        
    }else{
        
        NSDate *currentTime = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *resultString = [dateFormatter stringFromDate: currentTime];
        NSLog(@"Starting Order %@", currentTime);
        
        NSDateFormatter *dateFormatterL = [[NSDateFormatter alloc] init];
        [dateFormatterL setDateFormat:@"HH:mm a"];
        NSString *startLabel = [dateFormatterL stringFromDate: currentTime];
        
        
        // update status and time recieved.
        self.order.status = @"STARTED";
        // self.order.driverRecievedtTime =resultString;
        // self.order.customerName = resultString;
        self.order.startTime = resultString;
        
        self.startTime.text = startLabel;
        
        UIColor *greenColor =
        [UIColor colorWithRed:255.0f/255.0f green:149.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
        
        self.startTime.textColor = greenColor;
        
        //wrap order for location notification:
        
        NSDictionary* dict = [NSDictionary dictionaryWithObject:self.order
                                                         forKey:@"order"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderStartedNotification" object:self.order userInfo:dict];
        
        // notify delegate of update.
        
        [self.delegate controller:self didUpdateOrder:self.order];
        NSLog(@"Delegate Called");
        // start timer.
        
        //   myTicker = [NSTimer scheduledTimerWithTimeInterval: 1.0f target: self
        //                                  selector: @selector(updateTimeDisplay) userInfo: nil repeats: YES];
        
        // hide start button or place condition on start button <> OPEN.
        [self setButtonVisibility];
        [self startSpin];
        [self configureBg];
        
        [[OrderWatcher sharedEngine] startOrder:self.order];
        NSLog(@"Watcher orderInProgress property set to: %d", [OrderWatcher sharedEngine].orderInProgress);
    }

    
}




- (IBAction)startOrder:(id)sender {
   
    
    [self alertConfirmStart];
    
}


// an ivar for your class:
BOOL animating;



- (IBAction)stopOrder:(id)sender {
    
     [self alertConfirmComplete];
    
}

- (void) updateTimeDisplay{
    NSLog(@"Timer Int");
      count = count + 1;
    NSString *strFromInt = [NSString stringWithFormat:@"%d", count];
    
    NSLog(@"Count: %@", strFromInt);
    
    self.custName.text = strFromInt;
    
    

}

- (IBAction)dismiss:(id)sender {
    
    //[self dismissViewControllerAnimated:YES completion:nil];
}



-(void)configureDefaultBg{
    
    // Create the colors
    UIColor *darkOp =
    [UIColor colorWithRed:29.0f/255.0f green:122.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
    UIColor *lightOp =
    [UIColor colorWithRed:26.0f/255.0f green:214.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
    
    // Create the gradient
    layerDefaultBg = [CAGradientLayer layer];
    
    // Set colors
    layerDefaultBg.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    
    layerDefaultBg.locations = [NSArray arrayWithObjects:
                          [NSNumber numberWithFloat:0.0f],
                          [NSNumber numberWithFloat:1.4],
                          nil];
    
    // Set bounds
    layerDefaultBg.frame = self.parentView.bounds;
    
    // Add the gradient to the view
    [self.parentView.layer insertSublayer:layerDefaultBg atIndex:0];
    [layerStartedBg removeFromSuperlayer];
    [layerCompletedBg removeFromSuperlayer];
}

-(void)configureStartedBg{
    
    // Create the colors
    UIColor *darkOp =
    [UIColor colorWithRed:255.0f/255.0f green:88.0f/255.0f blue:64.0f/255.0f alpha:1.0f];
    UIColor *lightOp =
    [UIColor colorWithRed:255.0f/255.0f green:149.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
    
    // Create the gradient
    layerStartedBg = [CAGradientLayer layer];
    
    // Set colors
    layerStartedBg.colors = [NSArray arrayWithObjects:
                       (id)lightOp.CGColor,
                       (id)darkOp.CGColor,
                       nil];
    
    
    layerStartedBg.locations = [NSArray arrayWithObjects:
                          [NSNumber numberWithFloat:0.0f],
                          [NSNumber numberWithFloat:1.4],
                          nil];
    
    // Set bounds
    layerStartedBg.frame = self.parentView.bounds;
    
    // Add the gradient to the view
    [self.parentView.layer insertSublayer:layerStartedBg atIndex:0];
    
    [layerDefaultBg removeFromSuperlayer];
    [layerCompletedBg removeFromSuperlayer];
    
}

-(void)configureCompletedBg{
    
    // Create the colors
    UIColor *lightOp =
    [UIColor colorWithRed:135.0f/255.0f green:252.0f/255.0f blue:12.0f/255.0f alpha:1.0f];
  //  UIColor *darkOp =
  //  [UIColor colorWithRed:36.0f/255.0f green:212/255.0f blue:4.0f/255.0f alpha:1.0f];
    
    UIColor *darkOp =
      [UIColor colorWithRed:11.0f/255.0f green:212/255.0f blue:24.0f/255.0f alpha:1.0f];
    
    // Create the gradient
    layerCompletedBg = [CAGradientLayer layer];
    
    // Set colors
    layerCompletedBg.colors = [NSArray arrayWithObjects:
                             (id)darkOp.CGColor,
                             (id)darkOp.CGColor,
                             nil];
    
    
    layerCompletedBg.locations = [NSArray arrayWithObjects:
                                [NSNumber numberWithFloat:0.0f],
                                [NSNumber numberWithFloat:1.4],
                                nil];
    
    // Set bounds
    layerCompletedBg.frame = self.parentView.bounds;
    
    // Add the gradient to the view
    [self.parentView.layer insertSublayer:layerCompletedBg atIndex:0];
    
    [layerDefaultBg removeFromSuperlayer];
    [layerStartedBg removeFromSuperlayer];
    
}

-(void) alertOrderInProgress{
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delivery In Progress"
                                                    message:@"There is an existing delivery in progress. Please ensure existing deliveries are complete before starting new orders."
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];

}

-(void) alertConfirmStart{
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Start Order?"
                                                    message:@"Please confirm that you wish to start this order."
                                                   delegate:self
                                          cancelButtonTitle:@"Not Yet!"
                                          otherButtonTitles:@"Start", nil];
    [alert show];
    
}


-(void) alertConfirmComplete{
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Complete Order?"
                                                    message:@"Please confirm that you wish to complete this order."
                                                   delegate:self
                                          cancelButtonTitle:@"Not Yet!"
                                          otherButtonTitles:@"Complete", nil];
    [alert show];
    
}


#pragma mark - alert callback


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Start"])
    {
        [self startorderFromAlert];
    }else if ([title isEqualToString:@"Complete"]){
        
         [self completeOrderFromAlert];
    }
    
}


- (void) spinWithOptions: (UIViewAnimationOptions) options {
    // this spin completes 360 degrees every 2 seconds
    [UIView animateWithDuration: 0.5f
                          delay: 0.0f
                        options: options
                     animations: ^{
                         self.iconOrderStatus.transform = CGAffineTransformRotate(self.iconOrderStatus.transform, M_PI / 2);
                     }
                     completion: ^(BOOL finished) {
                         if (finished) {
                             if (animating) {
                                 // if flag still set, keep spinning with constant speed
                                 [self spinWithOptions: UIViewAnimationOptionCurveLinear];
                             } else if (options != UIViewAnimationOptionCurveEaseOut) {
                                 // one last spin, with deceleration
                                 [self spinWithOptions: UIViewAnimationOptionCurveEaseOut];
                             }
                         }
                     }];
}



- (void) startSpin {
    if (!animating) {
        animating = YES;
        [self spinWithOptions: UIViewAnimationOptionCurveEaseOut];
    }
}

- (void) stopSpin {
    // set the flag to stop spinning after one last 90 degree increment
    animating = NO;
}

- (void) rotateImageView{
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        [self.iconOrderStatus setTransform:CGAffineTransformRotate(self.iconOrderStatus.transform, M_PI_2)];
    }completion:^(BOOL finished){
        if (finished) {
            [self rotateImageView];
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
