//
//  TabBarController.h
//  DPOS Driver
//
//  Created by Simon Canil on 9/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface TabBarController : UITabBarController <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSMutableArray *geofences;



@end
