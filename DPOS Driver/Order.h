//
//  Order.h
//  DPOS Driver
//
//  Created by Simon Canil on 8/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Order : NSObject <NSCoding>

@property (nonatomic, strong) NSString *orderId;
@property (nonatomic, strong) NSString *runId;
@property (nonatomic, strong) NSString *customerName;
@property (nonatomic, strong) NSString *streetName;
@property (nonatomic, strong) NSString *postCode;
@property (nonatomic, strong) NSString *suburb;
@property (nonatomic, strong) NSString *inTime;
@property (nonatomic, strong) NSString *outTime;
@property (nonatomic, strong) NSString *quotedTime;
// add quoted time.
@property (nonatomic, strong) NSString *lat;
@property (nonatomic, strong) NSString *lon;
@property (nonatomic, strong) NSString *tenderAmount;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *driverRecievedtTime;
@property (nonatomic, strong) NSString *startTime;
@property (nonatomic, strong) NSString *endTime;
@property (nonatomic, strong) NSString *orderNotes;
@property (nonatomic, strong) NSString *paidAmount;
@property (nonatomic, strong) NSString *customerPhone;
@property (nonatomic, strong) NSString *responseCode;


@end
