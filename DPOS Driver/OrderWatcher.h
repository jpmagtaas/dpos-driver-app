//
//  OrderWatcher.h
//  DPOS Driver
//
//  Created by Simon Canil on 20/03/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Order.h"
#import "RestkitHelper.h"

@interface OrderWatcher : NSObject

@property (atomic, readonly) BOOL orderInProgress;
@property (atomic, readonly) Order *currentOrder;
@property RestkitHelper *restkitHelper;


+ (OrderWatcher *)sharedEngine;

- (void)stopOrder:(Order *) order;
- (void)startOrder:(Order *) order;
- (void)endRun:(Order *) order;

- (BOOL) isOrderInProgress;
- (Order *)getCurrentOrder;
@end
