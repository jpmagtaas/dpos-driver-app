//
//  Identity.h
//  DPOS Driver
//
//  Created by Simon Canil on 13/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Identity : NSObject  <NSCoding>

@property (nonatomic, strong) NSString *responseCode;
@property (nonatomic, strong) NSString *storeName;
@property (nonatomic, strong) NSString *lat;
@property (nonatomic, strong) NSString *lon;
@property (nonatomic, strong) NSString *driverName;
@property (nonatomic, strong) NSString *apiToken;

@end
