//
//  LocationShareModel.h
//  DPOS Driver
//
//  Created by Simon Canil on 6/10/2015.
//  Copyright © 2015 VisionApps. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "BackgroundTaskManager.h"
#import <CoreLocation/CoreLocation.h>

@interface LocationShareModel : NSObject

@property (nonatomic) NSTimer *timer;
@property (nonatomic) NSTimer * delay10Seconds;
@property (nonatomic) BackgroundTaskManager * bgTask;
@property (nonatomic) NSMutableArray *myLocationArray;
@property CLLocationCoordinate2D targetLocation;

+(id)sharedModel;

@end
