//
//  MapViewController.h
//  DPOS Driver
//
//  Created by Simon Canil on 8/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface MapViewController : UIViewController <CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *mapVIew;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UILabel *customerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *suburbLabel;
@property (weak, nonatomic) IBOutlet UILabel *etaLabel;
@property (weak, nonatomic) IBOutlet UILabel *quotedLabel;

@property (weak, nonatomic) IBOutlet UILabel *etaHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *quotedHeaderLabel;

@property (weak, nonatomic) IBOutlet UIVisualEffectView *footerPanel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *footHeight;

@property BOOL isReturningBase;

@end
