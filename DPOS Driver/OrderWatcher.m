//
//  OrderWatcher.m
//  DPOS Driver
//
//  Created by Simon Canil on 20/03/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import "OrderWatcher.h"
#import "RestkitHelper.h"
#import "Order.h"

@implementation OrderWatcher


+ (OrderWatcher *)sharedEngine {
    static OrderWatcher *sharedEngine = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedEngine = [[OrderWatcher alloc] init];
    });
    
    return sharedEngine;
}


- (void)startOrder: (Order *) order{
    
    [self willChangeValueForKey:@"orderInProgress"];
       _orderInProgress = YES;
       _currentOrder = order;
    
       
    [self didChangeValueForKey:@"orderInProgress"];
    
}
- (void)stopOrder: (Order *) order{
    
    [self willChangeValueForKey:@"orderInProgress"];
    _orderInProgress = NO;
    _currentOrder = nil;
    [self didChangeValueForKey:@"orderInProgress"];
}
- (void)endRun: (Order *) order{
    
    _orderInProgress = NO;
    _currentOrder = nil;
}

- (BOOL)isOrderInProgress{
    
    
     _restkitHelper = [[RestkitHelper alloc] init];
    BOOL orderInProgress = NO;
    
    [_restkitHelper loadOrders];
    NSLog(@"Found orders count %lu", [_restkitHelper.orders count]);
    for (int i = 0; i < [_restkitHelper.orders count]; i++) {
        
        Order *ord = [_restkitHelper.orders objectAtIndex:i];
        if ([ord.status isEqualToString: @"STARTED"]){
            
            orderInProgress = YES;
            
            _currentOrder = ord;
            
            NSLog(@"Order FOUND IN PROGRESS: %@ and run Id: %@", ord.customerName, ord.orderId);
        }else{
             _currentOrder = nil;
            
        }
        
    }
    
    return orderInProgress;
}

- (Order *)getCurrentOrder{
    
    
    _restkitHelper = [[RestkitHelper alloc] init];
    Order *currentOrder;
    
    [_restkitHelper loadOrders];
    NSLog(@"Found orders count %lu", [_restkitHelper.orders count]);
    for (int i = 0; i < [_restkitHelper.orders count]; i++) {
        
        Order *ord = [_restkitHelper.orders objectAtIndex:i];
        if ([ord.status isEqualToString: @"STARTED"]){
            
            
            currentOrder = ord;
            
            NSLog(@"Order FOUND IN PROGRESS: %@ and run Id: %@", ord.customerName, ord.orderId);
        }else{
            _currentOrder = nil;
            
        }
        
    }
    
    return currentOrder;
}

@end
