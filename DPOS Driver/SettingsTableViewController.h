//
//  SettingsTableViewController.h
//  DPOS Driver
//
//  Created by Simon Canil on 4/04/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menuButton;
@property (weak, nonatomic) IBOutlet UISwitch *autoCompleteOrderSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *autoCompleteRunSwitch;

@end
