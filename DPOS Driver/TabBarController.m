//
//  TabBarController.m
//  DPOS Driver
//
//  Created by Simon Canil on 9/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import "TabBarController.h"

#import "RestkitHelper.h"
#import "Identity.h"
#import "Order.h"
#import <MapKit/MapKit.h>

@interface TabBarController ()

@property Order *order;

@property RestkitHelper *restkitHelper;
@end


@implementation TabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // notification for reauth
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(authSuccess) name:@"AuthSavedNotification" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orderStartedCallBack:) name:@"OrderStartedNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orderStoppedCallBack:) name:@"OrderStopeedNotification" object:nil];
    
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(returnToRoot)];
    self.navigationItem.leftBarButtonItem = anotherButton;
    
    
    UIBarButtonItem *mapButton = [[UIBarButtonItem alloc] initWithTitle:@"Map" style:UIBarButtonItemStylePlain target:self action:@selector(showMapSegue)];
    self.navigationItem.rightBarButtonItem = mapButton;
    
    
    // load auth details which provides us store location lat lon:
    
    _restkitHelper = [[RestkitHelper alloc] init];
    
    [_restkitHelper loadAuth];
    
    Identity *identity = _restkitHelper.identities[0];
    
    NSLog(@"Store lat lon: %@ and %@", identity.lat, identity.lon);
   
    
    // set location monitor
    self.locationManager = [[CLLocationManager alloc] init];
    
    [[self locationManager] setDelegate:self];
    
    // we have to setup the location maanager with permission in later iOS versions
    if ([[self locationManager] respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [[self locationManager] requestAlwaysAuthorization];
          NSLog(@"Requesting always in use");
        
    }
    
    [[self locationManager] setDistanceFilter: 200];
    [[self locationManager] setDesiredAccuracy:kCLLocationAccuracyBest];
    [[self locationManager] startUpdatingLocation];
    [[self locationManager] setAllowsBackgroundLocationUpdates:YES];
    [[self locationManager] setPausesLocationUpdatesAutomatically:NO];
    
    // create geo fence
    
    MKCoordinateRegion region;
    
    region.center.latitude = 33.891747;
    region.center.longitude = 151.284259;
    
    CLLocationCoordinate2D centre;
    centre.latitude = region.center.latitude;
    centre.longitude = region.center.longitude;
    
    
    CLCircularRegion *regionMonitored = [[CLCircularRegion alloc] initWithCenter:centre
                                                                 radius:30.0
                                                             identifier:@"homeStore"];
    
    regionMonitored.notifyOnEntry = true;
    regionMonitored.notifyOnExit = true;
    
    //[self.locationManager startMonitoringForRegion:[[CLRegion alloc] initCircularRegionWithCenter:centre radius:20.0 identifier:@"Work"]];

    [self.locationManager startMonitoringForRegion:regionMonitored];
   
    // Load Geofences
    self.geofences = [NSMutableArray arrayWithArray:[[self.locationManager monitoredRegions] allObjects]];
    
    // find address. 
    
}


- (void) orderStartedCallBack:(NSNotification *)notification
{
    NSDictionary *dict = [notification userInfo];
    
    Order *o = [dict  objectForKey:@"order"];
    
    NSLog(@"****notified order: %@ lat %@ lon %@", o.customerName, o.lat, o.lon);
    
    // Start Monitoring destination GEOFENCE
    MKCoordinateRegion region;
    
    region.center.latitude = [o.lat doubleValue];
    region.center.longitude = [o.lon doubleValue];
    
    CLLocationCoordinate2D centre;
    centre.latitude = region.center.latitude;
    centre.longitude = region.center.longitude;
    
    
    CLCircularRegion *regionDestination = [[CLCircularRegion alloc] initWithCenter:centre
                                                                          radius:20.0
                                                                      identifier:@"destination"];

    [self.locationManager startMonitoringForRegion:regionDestination];
    
    
    NSLog(@"%@" ,self.locationManager.monitoredRegions);

    
}


- (void) orderStoppedCallBack:(NSNotification *)notification
{
    NSDictionary *dict = [notification userInfo];
    
    Order *o = [dict  objectForKey:@"order"];
    
    NSLog(@"****notified order stopped: %@ lat %@ lon %@", o.customerName, o.lat, o.lon);
    
    
}

- (void)showMapSegue{
    
    NSLog(@"show map");
}


-(void) authSuccess{
    // need to check counter here to prevent infinite looping if service not returning valid AUTH_TOKEN.
    // this block is only called upon reauth.
    
    _restkitHelper = [[RestkitHelper alloc] init];
    // [_restkitHelper loadOrders];
    [_restkitHelper configureRestKit];
    [_restkitHelper getOrders];
    

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"OK Complete"])
    {
        NSLog(@"End the order.");
    }
    
}

-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *location = locations.lastObject;
    NSLog(@"location hit %f", location.coordinate.latitude);
    
    NSString *lat = [NSString stringWithFormat:@"%f", location.coordinate.latitude];
    NSString *lon = [NSString stringWithFormat:@"%f", location.coordinate.longitude];
    
    _restkitHelper = [[RestkitHelper alloc] init];
    
    Order *o = [Order new];
    o.lat = lat;
    o.lon = lon;
    
    //[_restkitHelper getPostLocationResponse:o];
    
}

-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    NSLog(@"Welcome to %@", region.identifier);
    
    
    if ([region.identifier isEqualToString:@"destination"]){
        
        NSLog(@"@@@@@@@@DESTINATION REACHED %@", region.identifier);
        
    }

    
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    
    localNotification.alertBody = @"I'M IN THE REGION";
    
    localNotification.userInfo = [[NSDictionary alloc]initWithObjectsAndKeys:@"nearBy",@"type", nil];
    
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    
    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Complete Order?"
                                                    message:@"You have arrived at the destination for this order."
                                                   delegate:nil
                                          cancelButtonTitle:@"Not Yet!"
                                          otherButtonTitles:@"OK Complete", nil];
    [alert show];
    
    NSLog(@"@@@@@@@@ Showed Alert for: %@", region.identifier);

}


-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    NSLog(@"Bye bye");
    
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Exited"
                                                    message:@"By Bye"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

-(void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    NSLog(@"Now monitoring for %@", region.identifier);
    
    
    NSLog(@"%@" ,self.locationManager.monitoredRegions);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Monitoring Started"
                                                    message:@"GeoFence registered for home store."
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];

}




-(void)refreshPropertyList{
    
    
    NSLog(@"logout");
}

- (void) popTest {
  
    NSLog(@"logout");
}


- (void)returnToRoot {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
