//
//  OrderTableController.h
//  DPOS Driver
//
//  Created by Simon Canil on 8/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetail.h"
#import <CoreLocation/CoreLocation.h>
#import "SWRevealViewController.h"

@interface OrderTableController : UITableViewController <CLLocationManagerDelegate, UIAlertViewDelegate, SWRevealViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *menuButton;
@property (strong, nonatomic) CLLocationManager *locationManager;

@property (weak, nonatomic) IBOutlet UILabel *iconOrder;

-(void) updateOrder:(Order *)order;
@end
