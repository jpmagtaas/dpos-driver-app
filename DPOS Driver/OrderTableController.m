//
//  OrderTableController.m
//  DPOS Driver
//
//  Created by Simon Canil on 8/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import "OrderTableController.h"
#import "Order.h"
#import "Utils.h"
#import "RestkitHelper.h"
#import "TableViewCellOrders.h"
#import "SWRevealViewController.h"
#import "OrderWatcher.h"
#import "MapViewController.h"

#import <MapKit/MapKit.h>


@interface OrderTableController ()
@property RestkitHelper *restkitHelper;
@property Order *selection;
@property BOOL isCompleteOrderShown;

@end

@implementation OrderTableController


CAGradientLayer *layerDefaultBg;
CAGradientLayer *layerStartedBg;
CAGradientLayer *layerCompletedBg;

- (void)viewDidLoad {
    
    // [self performSegueWithIdentifier:@"login" sender:self];
   
    [super viewDidLoad];
    
    self.revealViewController.delegate = self;
    
    _restkitHelper = [[RestkitHelper alloc] init];
    
    [_restkitHelper configureRestKit];
    
    //menu icon
    
    [self.menuButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                             [UIFont fontWithName:@"fontawesome" size:22.0], NSFontAttributeName,
                                             [UIColor whiteColor], NSForegroundColorAttributeName,
                                             nil]
                                   forState:UIControlStateNormal];
    self.menuButton.title  = @"\uf0c9";
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTable) name:@"RestSuccessNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadRestData) name:@"PostOrderSuccessNotification" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadRestData) name:@"RunEndedNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(toggleSideNav) name:@"toggleSideNav" object:nil];
    
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadLocal) name:@"OrderStopeedNotification" object:nil];
    
    [self loadRestData];
    
    [self configureLayout];
    
    // Uncomment the following line to preserve selection between presentations.
     //self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
  
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = UITableViewAutomaticDimension;
      
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(void) toggleSideNav {
    [self.revealViewController revealToggleAnimated:YES];
}

-(void)returnToBase {
    UIStoryboard *story = [self storyboard];
    MapViewController *mapView = [story instantiateViewControllerWithIdentifier:@"mapNav"];
    UINavigationController *navControl = [[UINavigationController alloc] initWithRootViewController: mapView];
    navControl.modalPresentationStyle = UIModalPresentationPopover;
    navControl.modalTransitionStyle = UIModalPresentationPopover;
    
    [mapView setIsReturningBase:YES];
    
    [self presentViewController:navControl animated:YES completion:nil];
}

- (void) configureLayout{
    
    
    // map button
    UIBarButtonItem *mapButton = [[UIBarButtonItem alloc] initWithTitle:@"Map" style:UIBarButtonItemStylePlain target:self action:@selector(showMapSegue)];
    self.navigationItem.rightBarButtonItem = mapButton;
    
    //layout work
    
    self.parentViewController.title = @"Order Feeds";
    
    //pull to refresh
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor colorWithRed:26.0f/255.0f green:26.0f/255.0f blue:26.0f/255.0f alpha:1.0];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(refresh)
                  forControlEvents:UIControlEventValueChanged];
    
    // remove redundant seperators
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    //menu listener
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.menuButton setTarget: self.revealViewController];
        [self.menuButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
}

- (void)revealController:(SWRevealViewController *)revealController
      willMoveToPosition:(FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        //self.view.userInteractionEnabled = YES;
        self.tableView.userInteractionEnabled = YES;
    } else {
        //self.view.userInteractionEnabled = NO;
        self.tableView.userInteractionEnabled = NO;
    }
}

- (void)revealController:(SWRevealViewController *)revealController
       didMoveToPosition:(FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        //self.view.userInteractionEnabled = YES;
        self.tableView.userInteractionEnabled = YES;
    } else {
        //self.view.userInteractionEnabled = NO;
        self.tableView.userInteractionEnabled = NO;
    }
}


- (void)showMapSegue{
    
    NSLog(@"show map");
    [self performSegueWithIdentifier:@"map" sender:self];
}

// logout
- (void)returnToRoot {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
}


-(void) loadRestData {
    
    [_restkitHelper getOrders];
   
}


-(void) refresh {
    
    int completeCount = 0;
    NSLog(@"Found orders count in load:  %lu", [_restkitHelper.orders count]);
    for (int i = 0; i < [_restkitHelper.orders count]; i++) {
        
        Order *ord = [_restkitHelper.orders objectAtIndex:i];
        if ([ord.status isEqualToString: @"COMPLETED"]){
            
            completeCount = completeCount + 1;
            
            _selection = ord;
            
            NSLog(@"Order FOUND COMPLETED: %@ and run Id: %@", ord.customerName, ord.orderId);
        }else{
            NSLog(@"No orders in progress");
            
        }
        
    }
    
    
    if (completeCount > 0 && completeCount == [_restkitHelper.orders count]){
        [self alertConfirmEndRun];
        [self.refreshControl endRefreshing];
    }else{
        
        [_restkitHelper getOrders];
        [self.refreshControl endRefreshing];
        
    }
    
}

-(void) reloadLocal {
    
    _restkitHelper = [[RestkitHelper alloc] init];
    [_restkitHelper loadOrders];
    
    
    [self.tableView reloadData];
    
}


-(void) reloadTable {
    
    [self.tableView reloadData];
     NSLog(@"########table reloaded");
    
   [self checkAllCompletedOrders];
}

-(void) checkAllCompletedOrders {
    int completeCount = 0;
    NSLog(@"Found orders count in load:  %lu", [_restkitHelper.orders count]);
    for (int i = 0; i < [_restkitHelper.orders count]; i++) {
        
        Order *ord = [_restkitHelper.orders objectAtIndex:i];
        if ([ord.status isEqualToString: @"COMPLETED"]){
            
            completeCount = completeCount + 1;
            
        }else{
            NSLog(@"No orders in progress");
            
        }
        
    }
    
    
    if (completeCount > 0 && completeCount == [_restkitHelper.orders count]){
        
        if(!self.isCompleteOrderShown) {
            [self alertReturnHome];
            self.isCompleteOrderShown = YES;
        }
        
    }
}




- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        // Set Title
        self.title = @"Order Feed";
        
        // Load Items
        [_restkitHelper loadOrders];
    }
    
    return self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSLog(@"Order Count: %lu",_restkitHelper.orders.count );
    return _restkitHelper.orders.count;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    return UITableViewAutomaticDimension;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TableViewCellOrders *cell = [tableView dequeueReusableCellWithIdentifier:@"orderCell" forIndexPath:indexPath];
    
    Order *order = [_restkitHelper.orders objectAtIndex:[indexPath row]];
    
    cell.customerName.text = order.customerName;
    cell.orderNumber.text = order.orderId;
    cell.address.text = order.streetName;
    
    //format order in time = current time - order in time. This will represent time customer is waiting.
    
    NSString *dateString = order.quotedTime;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
   [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:dateString];
    NSDate *today = [NSDate date];
    
    // difference between now and quoted time
    NSTimeInterval timeInterval = [today timeIntervalSinceDate:dateFromString];
    cell.orderAge.text = [[[Utils stringMinutesFromTimeInterval:timeInterval] stringByAppendingString:@" mins"] stringByReplacingOccurrencesOfString:@"-" withString:@""];
    //change color if positive interval (ie overdue).
    
    
    //set icon.
    [cell.iconOrder setFont:[UIFont fontWithName:@"fontawesome" size:20]];
    cell.iconOrder.text = @"\uf00c";
    
    
    //hide order icon overdue and set to exclamation mark for later unhide.
    [cell.iconOverdue setFont:[UIFont fontWithName:@"fontawesome" size:20]];
    cell.iconOverdue.text = @"\uf12a";
    cell.iconOverdue.hidden = YES;
    
    
    
    //Status based formatting.
    if ([order.status isEqualToString:@"COMPLETED"]) {
        
        cell.iconOrder.text = @"\uf058";
        cell.orderAge.text = @"Completed";
        
        [cell setBackgroundColor:[UIColor clearColor]];
        
        [cell setBackgroundColor:[UIColor colorWithRed:11.0/255.0 green:225.0/255.0 blue:24.0/255.0 alpha:1.0]];
        
    } else if ([order.status isEqualToString:@"STARTED"]){
        
        cell.iconOrder.text = @"\uf1b9";
        [cell setBackgroundColor:[UIColor colorWithRed:253.0/255.0 green:142.0/255.0 blue:0.0/255.0 alpha:1.0]];
    } else{
        
        cell.iconOrder.text = @"\uf019";
        
        CAGradientLayer *grad = [CAGradientLayer layer];
        grad.frame = CGRectMake(0, 0, tableView.bounds.size.width, cell.bounds.size.height);
        
        
        //override formatting for overdue orders:
        
        if (timeInterval > 0){
            
            [cell setBackgroundColor:[UIColor colorWithRed:230.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0]];
            cell.iconOverdue.hidden = NO;
            
        }else{
            [cell setBackgroundColor:[UIColor colorWithRed:23.0/255.0 green:152.0/255.0 blue:255.0/255.0 alpha:1.0]];
        }
    
    }
    
    
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Order *order = [_restkitHelper.orders objectAtIndex:[indexPath row]];
    
    if (order) {
        // Update Selection
        [self setSelection:order];
        [self performSegueWithIdentifier:@"detail" sender:self];
    }

    
}


- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    // Fetch Item
    Order *order = [_restkitHelper.orders objectAtIndex:[indexPath row]];
    
    if (order) {
        // Update Selection
        [self setSelection:order];
         [self performSegueWithIdentifier:@"detail" sender:self];
    }
    
}


#pragma mark - Slide out table cell buttons
// REF: gist.github.com/marksands/76558707f583dbb8f870

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {

    
    _selection = [_restkitHelper.orders objectAtIndex:[indexPath row]];
    
    UITableViewRowAction *startAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Start" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        // maybe show an action sheet with more options
        [self.tableView setEditing:NO];
        if ([OrderWatcher sharedEngine].isOrderInProgress == YES){
            
            [self alertOrderInProgress];
            
        }else{
            [self alertConfirmStart];
        }
        
    }];
    // moreAction.backgroundColor = [UIColor lightGrayColor];
    [startAction setBackgroundEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];
    
    UITableViewRowAction *stopAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Complete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        //   [self.objects removeObjectAtIndex:indexPath.row];
        [self.tableView setEditing:NO];
            [self alertConfirmComplete];
        

    }];
    startAction.backgroundColor =  [UIColor colorWithRed:26.0f/255.0f green:214.0f/255.0f blue:243.0f/255.0f alpha:1.0f];

    UITableViewRowAction *resetAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Reset" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        // maybe show an action sheet with more options
        
        [self.tableView setEditing:NO];
         [self alertConfirmReset];
        
    }];
    
    // moreAction.backgroundColor = [UIColor lightGrayColor];
    resetAction.backgroundColor =  [UIColor colorWithRed:255.0f/255.0f green:221.0f/255.0f blue:0.0f/255.0f alpha:1.0];
    if ([_selection.status isEqualToString:@"COMPLETED"]) {
      
          return @[resetAction];
    }else if ([_selection.status isEqualToString:@"STARTED"]) {
        
        
        return @[stopAction];

    }else  {
    
        
        return @[startAction];
    }
    
}

// From Master/Detail Xcode template
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //   [self.objects removeObjectAtIndex:indexPath.row];
       // [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}



-(void) updateOrder:(Order *)order {
    
    
    for (int i = 0; i < [_restkitHelper.orders count]; i++) {
        Order *ord = [_restkitHelper.orders objectAtIndex:i];
        
        if ([[ord orderId] isEqualToString:[order orderId]]) {
            // Update Table View Row
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            NSLog(@"FOUND ORDER _ UPDATING for start time: %@", order.startTime);
            
            
        }
    }
    
    // Save Items
    [_restkitHelper saveOrders];
    NSLog(@"FNISHED EDITING ORDER HERE");
    
    [self reloadTable];

}


-(void) startOrderFromAlert{

        
        NSDate *currentTime = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *resultString = [dateFormatter stringFromDate: currentTime];
        NSLog(@"Starting Order %@", currentTime);
        
        NSDateFormatter *dateFormatterL = [[NSDateFormatter alloc] init];
        [dateFormatterL setDateFormat:@"HH:mm a"];
    
        
        // update status and time recieved.
        _selection.status = @"STARTED";
        _selection.startTime = resultString;
    
        NSDictionary* dict = [NSDictionary dictionaryWithObject:_selection
                                                     forKey:@"order"];
    
        [self updateOrder:_selection];
    
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderStartedNotification" object:_selection userInfo:dict];

    
    
        [[OrderWatcher sharedEngine] startOrder:_selection];
    
}


-(void) completeOrderFromAlert{
    
    
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *resultString = [dateFormatter stringFromDate: currentTime];
    NSLog(@"Stopping Order %@", currentTime);
    
    // update status and time recieved.
    _selection.status = @"COMPLETED";
    _selection.endTime = resultString;
    
    NSDictionary* dict = [NSDictionary dictionaryWithObject:_selection
                                                     forKey:@"order"];

    [self updateOrder:_selection];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderStopeedNotification" object:_selection userInfo:dict];
    
    
    
    [[OrderWatcher sharedEngine] stopOrder:_selection];
}

-(void) resetOrderFromAlert{
    
    

    // update status and time recieved.
    _selection.status = @"ALLOCATED";
    _selection.endTime = @"";
    _selection.startTime = @"";
    
    NSDictionary* dict = [NSDictionary dictionaryWithObject:_selection
                                                     forKey:@"order"];
    
    [self updateOrder:_selection];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderResetNotification" object:_selection userInfo:dict];
    
    
}


-(void) endRunFromAlert{
    
    
    Order *o = _selection;
    
    [_restkitHelper getPostRunResponse:o];
    _selection = nil;
    
    
}

- (void)controller:(OrderDetail *)controller didUpdateOrder:(Order *)order {
    // Fetch Item
    [self updateOrder:order];
}

#pragma mark - alert callback


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Start"])
    {
        [self startOrderFromAlert];
    }else if ([title isEqualToString:@"Complete"]){
        
        [self completeOrderFromAlert];
    }else if ([title isEqualToString:@"Reset"]){
        
        
        [self resetOrderFromAlert];
    }else if ([title isEqualToString:@"End Run"]){
        
        
        [self endRunFromAlert];
    } else if([title isEqualToString:@"Go"]) {
        [self returnToBase];
        
    }
    
}

#pragma mark - alerts


-(void) alertConfirmStart{
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Start Order?"
                                                    message:@"Please confirm that you wish to start this order."
                                                   delegate:self
                                          cancelButtonTitle:@"Not Yet!"
                                          otherButtonTitles:@"Start", nil];
    [alert show];
    
}


-(void) alertConfirmComplete{
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Complete Order?"
                                                    message:@"Please confirm that you wish to complete this order."
                                                   delegate:self
                                          cancelButtonTitle:@"Not Yet!"
                                          otherButtonTitles:@"Complete", nil];
    [alert show];
    
}

-(void) alertConfirmReset{
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reset Order?"
                                                    message:@"Please confirm that you wish to reset this order. Start and stop times will be reset."
                                                   delegate:self
                                          cancelButtonTitle:@"Not Yet!"
                                          otherButtonTitles:@"Reset", nil];
    [alert show];
    
}

-(void) alertReturnHome {
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"All orders are delivered"
                                                    message:@"Would you like to return to base?"
                                                   delegate:self
                                          cancelButtonTitle:@"Not Yet!"
                                          otherButtonTitles:@"Go", nil];
    [alert show];
    
}

-(void) alertConfirmEndRun{
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"End Run?"
                                                    message:@"Please confirm that you wish to end this run and look for more orders."
                                                   delegate:self
                                          cancelButtonTitle:@"Not Yet!"
                                          otherButtonTitles:@"End Run", nil];
    [alert show];
    
}

-(void) alertOrderInProgress{
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delivery In Progress"
                                                    message:@"There is an existing delivery in progress. Please ensure existing deliveries are complete before starting new orders."
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"detail"]) {
        // Fetch Edit Item View Controller
        OrderDetail *vc = (OrderDetail *)segue.destinationViewController;
        
  
        [vc setOrder:self.selection];
        
       
        // Set Delegate
        [vc setDelegate:self];
        
        
    }
    
}


@end
