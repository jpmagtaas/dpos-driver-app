//
//  DriverFeedNavigationController.m
//  DPOS Driver
//
//  Created by Simon Canil on 13/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import "DriverFeedNavigationController.h"

#import "RestkitHelper.h"
#import "Identity.h"
#import "Order.h"
#import "OrderTableController.h"
#import <MapKit/MapKit.h>
#import "OrderWatcher.h"
#import "Utils.h"
#import "LocationShareModel.h"

@interface DriverFeedNavigationController ()

@property Order *order;
@property BOOL isInTargetRegion;
@property CLCircularRegion *regionMonitored;


@property RestkitHelper *restkitHelper;

@property NSString *runId;

@end

@implementation DriverFeedNavigationController

Order *enteredOrder;

- (void)viewDidLoad {
    
   
  
    [super viewDidLoad];
    
    // notification for reauth
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(authSuccess) name:@"AuthSavedNotification" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orderStartedCallBack:) name:@"OrderStartedNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orderStoppedCallBack:) name:@"OrderStopeedNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orderResetCallBack:) name:@"OrderResetNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(endRunCallBack:) name:@"EndRunNotification" object:nil];
    
    
    
    
    // load auth details which provides us store location lat lon:
    
    _restkitHelper = [[RestkitHelper alloc] init];
    
    [_restkitHelper loadAuth];
    
    Identity *identity = _restkitHelper.identities[0];
    
    NSLog(@"Store lat lon: %@ and %@", identity.lat, identity.lon);
    
    
    // set location monitor
    self.locationManager = [[CLLocationManager alloc] init];
    
    [[self locationManager] setDelegate:self];
    
    // we have to setup the location maanager with permission in later iOS versions
    if ([[self locationManager] respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [[self locationManager] requestAlwaysAuthorization];
        NSLog(@"Requesting always in use");
        
    }
    
    [[self locationManager] setDistanceFilter: 30];
    [[self locationManager] setDesiredAccuracy:kCLLocationAccuracyBest];
    [[self locationManager] startUpdatingLocation];
    [[self locationManager] setAllowsBackgroundLocationUpdates:YES];
    [[self locationManager] setPausesLocationUpdatesAutomatically:NO];
    
    // create geo fence for store location.
    
    MKCoordinateRegion region;
    
    region.center.latitude = [identity.lat doubleValue];
    region.center.longitude = [identity.lon doubleValue];
    
    CLLocationCoordinate2D centre;
    centre.latitude = region.center.latitude;
    centre.longitude = region.center.longitude;
    
    
    self.regionMonitored = [[CLCircularRegion alloc] initWithCenter:centre
                                                                          radius:30.0
                                                                      identifier:@"homeStore"];
    
    self.regionMonitored.notifyOnEntry = YES;
    self.regionMonitored.notifyOnExit = YES;
    
    [self.locationManager startMonitoringForRegion:self.regionMonitored];
    
    // Load Geofences
    self.geofences = [NSMutableArray arrayWithArray:[[self.locationManager monitoredRegions] allObjects]];
    
    // find address.

    
    
    
}




- (void) orderStartedCallBack:(NSNotification *)notification
{
    NSDictionary *dict = [notification userInfo];
    
    Order *o = [dict  objectForKey:@"order"];
    
    NSLog(@"****notified order: %@ lat %@ lon %@", o.customerName, o.lat, o.lon);
    
    // Start Monitoring destination GEOFENCE
    MKCoordinateRegion region;
    
    region.center.latitude = [o.lat doubleValue];
    region.center.longitude = [o.lon doubleValue];
    
    CLLocationCoordinate2D centre;
    centre.latitude = region.center.latitude;
    centre.longitude = region.center.longitude;
    
    
    CLCircularRegion *regionDestination = [[CLCircularRegion alloc] initWithCenter:centre
                                                                            radius:20.0
                                                                        identifier:o.orderId];
    
    [self.locationManager startMonitoringForRegion:regionDestination];
    
    
    NSLog(@"%@" ,self.locationManager.monitoredRegions);
    
    [_restkitHelper getPostOrderResponse:o];
}


- (void) orderStoppedCallBack:(NSNotification *)notification
{
    
    NSLog(@">>>>> Order stopped callback");
    NSDictionary *dict = [notification userInfo];
    
    Order *o = [dict  objectForKey:@"order"];
    
    [self checkAllCompletedOrder:o];
    
    NSLog(@"****notified order stopped: %@ lat %@ lon %@", o.customerName, o.lat, o.lon);
    
    // unload geofence
    // Start Monitoring destination GEOFENCE
    MKCoordinateRegion region;
    
    region.center.latitude = [o.lat doubleValue];
    region.center.longitude = [o.lon doubleValue];
    
    CLLocationCoordinate2D centre;
    centre.latitude = region.center.latitude;
    centre.longitude = region.center.longitude;
    
    
    CLCircularRegion *regionDestination = [[CLCircularRegion alloc] initWithCenter:centre
                                                                            radius:20.0
                                                                        identifier:o.orderId];
    [self.locationManager stopMonitoringForRegion:regionDestination];
    
    [self.locationManager requestStateForRegion:self.regionMonitored];

    
}

-(void) checkAllCompletedOrder:(Order *)o {
    // get orders
     BOOL allOrdersComplete = YES;
    
    int count = 0;
    
     [_restkitHelper loadOrders];
     NSLog(@"ORders found upon arrival %lu", [_restkitHelper.orders count]);
     for (int i = 0; i < [_restkitHelper.orders count]; i++) {
        
         Order *ord = [_restkitHelper.orders objectAtIndex:i];
         if (![ord.status isEqualToString: @"COMPLETED"]){
             
             allOrdersComplete = NO;
                          
             NSLog(@"OrderID: %@ and run Id: %@", ord.orderId, ord.runId);
         } else {
             count = count + 1;
         }
     
     }
    
    NSLog(@"Order count %lu and Completed orders %d", (unsigned long)[_restkitHelper.orders count], count);
     
     if (allOrdersComplete || count == ([_restkitHelper.orders count] - 1)){
         
         //get home store marker
         _restkitHelper = [[RestkitHelper alloc] init];
         [_restkitHelper loadAuth];
         
         Identity *homeStoreLocation = _restkitHelper.identities[0];
         
         //set location to navigate to
         CLLocationCoordinate2D location;
         location.latitude = [homeStoreLocation.lat doubleValue];
         location.longitude =[homeStoreLocation.lon doubleValue];
         
         //create placemark
         MKPlacemark* placemark = [[MKPlacemark alloc] initWithCoordinate:location addressDictionary:nil];
         MKMapItem* destinationMapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
         
         //construct direction request
         MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
         [request setSource:[MKMapItem mapItemForCurrentLocation]];
         [request setDestination:destinationMapItem];
         [request setTransportType:MKDirectionsTransportTypeAutomobile]; // This can be limited to automobile and walking directions.
         [request setRequestsAlternateRoutes:NO]; // Gives you several route options. Switched OFF - first route item chosen.
         
         //send request.
         MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
         
         __block NSInteger ti = 0;
         
         [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
             if (!error) {
                 
                 MKRoute *route1 = [[response routes] objectAtIndex:0];
                 
                 ti = route1.expectedTravelTime;
                 [_restkitHelper getLastPostOrderResponse:o eta:[NSNumber numberWithInteger:ti]];
             
             } else {
                 [_restkitHelper getLastPostOrderResponse:o eta:@0];
             }
         }];
         
         
     }else{
         NSLog(@"Orders still remaining don't send ETA");
         [_restkitHelper getPostOrderResponse:o];
         
     }
}


- (void) endRunCallBack:(NSNotification *)notification
{
    
    NSLog(@">>>>> Order reset callback");
    NSDictionary *dict = [notification userInfo];
    
    Order *o = [dict  objectForKey:@"order"];
    
    [_restkitHelper getPostResetOrderResponse:o];
    
    
}


- (void) orderResetCallBack:(NSNotification *)notification
{
    
    NSLog(@">>>>> Order reset callback");
    NSDictionary *dict = [notification userInfo];
    
    Order *o = [dict  objectForKey:@"order"];
    
    [_restkitHelper getPostResetOrderResponse:o];
    
    
}

- (void)showMapSegue{
    NSLog(@"show map");
}


-(void) authSuccess{
    // need to check counter here to prevent infinite looping if service not returning valid AUTH_TOKEN.
    // this block is only called upon reauth.
    
    _restkitHelper = [[RestkitHelper alloc] init];
    // [_restkitHelper loadOrders];
    [_restkitHelper configureRestKit];
    [_restkitHelper getOrders];
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Complete Order"])
    {
        NSLog(@"End the order.");
        [self completeOrderFromAlert];
        
    
    }
    if([title isEqualToString:@"Complete Run"])
    {
      //  NSLog(@"End the run.");
        NSLog(@"Completing run for %@", _runId);
       // commented out now being updated automatically
        //[_restkitHelper getPostRunResponse:_runId];
        
        
    }
    
}


-(void) completeOrderFromAlert{
    
    NSLog(@"COMPLETING ORDER FOR %@ which started: %@", enteredOrder.orderId, enteredOrder.startTime);
    
    enteredOrder.status = @"COMPLETED";
    
    
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *resultString = [dateFormatter stringFromDate: currentTime];
    NSLog(@"Stopping Order %@", currentTime);
    
    // update status and time recieved.
    enteredOrder.status = @"COMPLETED";
    enteredOrder.endTime = resultString;
    
    NSDictionary* dict = [NSDictionary dictionaryWithObject:enteredOrder
                                                     forKey:@"order"];
    
  //  [self updateOrder:enteredOrder];
    
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderStopeedNotification" object:enteredOrder userInfo:dict];
    
    
    
    [[OrderWatcher sharedEngine] stopOrder:enteredOrder];
    
   // [self dismissViewControllerAnimated:YES completion:nil];
    
    for (UIViewController *childViewController in [self childViewControllers])
    {
        if ([childViewController isKindOfClass:[OrderTableController class]])
        {
            
            NSLog(@"Found Order Table Controller");
            
            [childViewController.navigationController popToRootViewControllerAnimated:YES];
            
            break;
        }
    
  
    }
    NSLog(@"POPPED VC");
    
}

-(void) updateOrder:(Order *)order {
    
    
    for (int i = 0; i < [_restkitHelper.orders count]; i++) {
        Order *ord = [_restkitHelper.orders objectAtIndex:i];
        
        if ([[ord orderId] isEqualToString:[order orderId]]) {
            // Update Table View Row
                //   [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            NSLog(@"FOUND ORDER _ UPDATING for start time: %@", order.startTime);
         //   [self popToRootViewControllerAnimated:NO];
            ord.status = @"COMPLETED";
            ord.startTime = order.startTime;

        }
    }
    
    // Save Items
    [_restkitHelper saveOrders];
    [_restkitHelper loadOrders];
    NSLog(@"FNISHED EDITING ORDER HERE");
    
  //  [self reloadTable];
    
}


-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *location = locations.lastObject;
    NSLog(@"location hit %f", location.coordinate.latitude);
    
    NSString *lat = [NSString stringWithFormat:@"%f", location.coordinate.latitude];
    NSString *lon = [NSString stringWithFormat:@"%f", location.coordinate.longitude];
    
    _restkitHelper = [[RestkitHelper alloc] init];
    
    Order *o = [Order new];
    
    o.lat = lat;
    o.lon = lon;
    
    CLLocationCoordinate2D target = [[LocationShareModel sharedModel] targetLocation];
    
    [self updateMapStatus:target order:o];
    
}

-(void)updateMapStatus: (CLLocationCoordinate2D) target order:(Order *)order {
    //create placemark
    MKPlacemark* placemark = [[MKPlacemark alloc] initWithCoordinate:target addressDictionary:nil];
    MKMapItem* destinationMapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    
    //construct direction request
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    [request setSource:[MKMapItem mapItemForCurrentLocation]];
    [request setDestination:destinationMapItem];
    [request setTransportType:MKDirectionsTransportTypeAutomobile]; // This can be limited to automobile and walking directions.
    [request setRequestsAlternateRoutes:NO]; // Gives you several route options. Switched OFF - first route item chosen.
    
    //send request.
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
    
    [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        if (!error) {
            MKRoute *route1 = [[response routes] objectAtIndex:0];
            
            NSString *eta = [Utils stringFromTimeIntervalHourMinutes:route1.expectedTravelTime];
            
            NSInteger ti = route1.expectedTravelTime;
            
            NSLog(@"ETA route1 coming from Driver Feed: %@", [Utils stringFromTimeIntervalHourMinutes:route1.expectedTravelTime]);
            
            NSString *distance = [[NSString alloc] init];
            
            if(route1.distance > 0) {
                distance = [NSString stringWithFormat:@"%.2f", route1.distance / 1000];
            }
            
            [_restkitHelper getPostLocationResponse:order eta:[NSNumber numberWithInteger:ti] distance:[NSNumber numberWithDouble:[distance doubleValue]]];
            
        }
    }];

}

-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    NSLog(@"@@@@@@@@DESTINATION REACHED %@", region.identifier);
    

    if ([region.identifier isEqualToString:@"homeStore"]){
        
        [self homeStoreArrival];
    }else {
        NSLog(@"Welcome to %@", region.identifier);

        NSString *enteredOrderId = region.identifier;
        
        [_restkitHelper loadOrders];
        
        NSLog(@"Looping through %lu orders", [_restkitHelper.orders count]);
        
        for (int i = 0; i < [_restkitHelper.orders count]; i++) {
            Order *ord = [_restkitHelper.orders objectAtIndex:i];
            
            if ([[ord orderId] isEqualToString:enteredOrderId]) {
                NSLog(@"FOUND region order_id %@ and start time %@", ord.orderId, ord.startTime);
                enteredOrder = ord;
                NSLog(@"Setting entered order to %@ and start time %@", enteredOrder.orderId, enteredOrder.startTime);
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Complete Order?"
                                                                message:@"You have arrived at the destination for this order."
                                                               delegate:self
                                                      cancelButtonTitle:@"Not Yet!"
                                                      otherButtonTitles:@"Complete Order", nil];
                [alert show];
                
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];

                localNotification.alertBody = @"Complete Order?";
                
                localNotification.userInfo = [[NSDictionary alloc]initWithObjectsAndKeys:@"nearBy",@"type", nil];
                
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                
                [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
                NSLog(@"@@@@@@@@ Showed Alert for: %@", region.identifier);
                
            }
        }
        
    }
    
}


- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    if (state == CLRegionStateInside){
        NSLog(@"is in target region");
        self.isInTargetRegion = YES;
        [self homeStoreArrival];
    }else{
        NSLog(@"is out of target region");
        self.isInTargetRegion  = NO;
        
    
    }

}

-(void) homeStoreArrival{
    
    
    // get orders
    BOOL allOrdersComplete = YES;
    
    if([_restkitHelper.orders count] == 0) {
        allOrdersComplete = NO;
    }
   
    [_restkitHelper loadOrders];
    NSLog(@"ORders found upon arrival %lu", [_restkitHelper.orders count]);
    for (int i = 0; i < [_restkitHelper.orders count]; i++) {
       
        Order *ord = [_restkitHelper.orders objectAtIndex:i];
        if (![ord.status isEqualToString: @"COMPLETED"]){
            
            allOrdersComplete = NO;
            
            _runId = ord.runId;
            
            NSLog(@"OrderID: %@ and run Id: %@", ord.orderId, ord.runId);
        }else{
            _runId = ord.runId;
            
        }
    
    }
    
    
    NSLog(@"All orders Complete? %d" , allOrdersComplete);
    
    if (allOrdersComplete){
    
    
        Order *o = [Order new];
        o.runId = _runId;
        [_restkitHelper getPostRunResponse:o];
        [_restkitHelper clearOrders];
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Run Complete"
                                                    message:@"You have completed this order run. Well done."
                                                   delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
        [alert show];
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
      
        localNotification.alertBody = @"Order Run Complete";
    
        localNotification.userInfo = [[NSDictionary alloc]initWithObjectsAndKeys:@"nearBy",@"type", nil];
    
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
    
        localNotification.soundName = UILocalNotificationDefaultSoundName;
    
        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
        
        
        

    }else{
        
        
        NSLog(@"orders still remaining");
        
         /*
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order Run Finished WITH FORCE?"
                                                        message:@"You have arrived back at your home store. Complete Order Run?"
                                                       delegate:nil
                                              cancelButtonTitle:@"Not Yet!"
                                              otherButtonTitles:@"FORCE COMPLETE Run", nil];
     //   [alert show];
     UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        
        localNotification.alertBody = @"Order Run Complete";
        
        localNotification.userInfo = [[NSDictionary alloc]initWithObjectsAndKeys:@"nearBy",@"type", nil];
        
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        
        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];

        */
        
    }
    
    
}


-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    NSLog(@"Bye bye");
    
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Exited"
                                                    message:@"By Bye"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
   // [alert show];
}

-(void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    NSLog(@"Now monitoring for %@", region.identifier);
    
    
    NSLog(@"%@" ,self.locationManager.monitoredRegions);
    
}





-(void)refreshPropertyList{
    
    
    NSLog(@"logout");
}

- (void) popTest {
    
    NSLog(@"logout");
}


- (void)returnToRoot {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
