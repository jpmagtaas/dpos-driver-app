//
//  ViewController.m
//  DPOS Driver
//
//  Created by Simon Canil on 8/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import "ViewController.h"
#import "RestkitHelper.h"
#import "KeychainItemWrapper.h"

@interface ViewController ()


@property RestkitHelper *restkitHelper;


@end

@implementation ViewController

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    
    // register call backs from serice calls - auth failed and auth succeeded.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(authSuccess) name:@"AuthSavedNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(authFail) name:@"AuthFailedNotification" object:nil];

 
    // set up page
    
    [self configureLayout];
    
    
    // auto login switch listener
    
    [self.autoLoginSwitch addTarget:self
                             action:@selector(stateChanged:) forControlEvents:UIControlEventValueChanged];

    
}



- (void) configureLayout{
    
    
   // self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"crust-background"]];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"dposBg1x"]];

    
    // Do any additional setup after loading the view, typically from a nib.
    
    self.userId.delegate = self;
    self.password.delegate = self;
    self.storeId.delegate = self;
    
    self.password.returnKeyType = UIReturnKeyDone;
    self.userId.returnKeyType = UIReturnKeyDone;
    self.storeId.returnKeyType = UIReturnKeyDone;
    
    [self populateCredentials];
    
    
    // populate autologin switch
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *autoLogin = [defaults objectForKey:@"autoLogin"];
    
    
    if ([autoLogin isEqualToString:@"Y"]){
        
        [self.autoLoginSwitch setOn:YES animated:NO];
        
        [self loadRestAuth];
        
    } else{
        
        [self.autoLoginSwitch setOn:NO animated:NO];
    }
    
    
    
}


- (void) populateCredentials{
    
    KeychainItemWrapper *keychain =
    [[KeychainItemWrapper alloc] initWithIdentifier:@"VisionKeyChain" accessGroup:nil];
    self.password.text = [keychain objectForKey:(__bridge id)kSecValueData];
    self.userId.text =[keychain objectForKey:(__bridge id)kSecAttrAccount];
    self.storeId.text =[keychain objectForKey:(__bridge id)kSecAttrDescription];
    

    
}


- (void)stateChanged:(UISwitch *)switchState
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([switchState isOn]) {
        self.autoLoginLabel.text = @"Auto Login On";
        [defaults setObject:@"Y" forKey:@"autoLogin"];
        
    } else {
        self.autoLoginLabel.text = @"Auto Login Off";
        [defaults setObject:@"N" forKey:@"autoLogin"];

    }
    
    [defaults synchronize];
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

// It is important for you to hide kwyboard

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void) authSuccess{
    
    [_restkitHelper clearOrders];
    
      [self performSegueWithIdentifier: @"login" sender: self];
}
- (void) authFail{
    //alert
    NSLog(@"Failed Auth");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Authentication Error"
                                                    message:@"Credentials provided are invalid. Please correct."
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
}





- (IBAction)toggleAutoLogin:(id)sender {
    
   
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *autoLogin = [defaults objectForKey:@"autoLogin"];
    
    if ([autoLogin isEqualToString:@"Y"]){
        [defaults setObject:@"N" forKey:@"autoLogin"];
        
    }else if ([autoLogin isEqualToString:@"N"]){
        [defaults setObject:@"Y" forKey:@"autoLogin"];
    } else{
        
        [defaults setObject:@"Y" forKey:@"autoLogin"];
    }
    
    [defaults synchronize];
    
    NSLog(@"auto login value: %@", [defaults objectForKey:@"autoLogin"]);
}







-(void) loadRestAuth {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    _restkitHelper = [[RestkitHelper alloc] init];
  //  [_restkitHelper loadOrders];
    
    NSString *autoLogin = [defaults objectForKey:@"autoLogin"];
    
    if ([autoLogin isEqualToString:@"Y"]){
    [_restkitHelper configureRestKit];
        [_restkitHelper getAuth:self.storeId.text :self.userId.text :self.password.text :@"N"];
        NSLog(@"Attempting auto login with stored credentials");
    }
    
}

- (IBAction)go:(id)sender {
    
     _restkitHelper = [[RestkitHelper alloc] init];
   //   [_restkitHelper loadOrders];
    
    [_restkitHelper configureRestKit];
    [_restkitHelper getAuth:self.storeId.text :self.userId.text :self.password.text :@"N"];
        NSLog(@"Attempting auto login with stored credentials");
}
    
    



// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    NSString *userName = self.userId.text;
    NSString *storeId = self.storeId.text;
    NSString *password = self.password.text;
    
    if ([segue.identifier isEqualToString:@"login"]) {
        // Fetch Edit Item View Controller
        NSLog(@"Before Segue");
        
        // Store username to keychain
        KeychainItemWrapper *keychain =
        [[KeychainItemWrapper alloc] initWithIdentifier:@"VisionKeyChain" accessGroup:nil];

        [keychain setObject:userName forKey:(__bridge id)kSecAttrAccount];
        [keychain setObject:password forKey:(__bridge id)kSecValueData];
        [keychain setObject:storeId forKey:(__bridge id)kSecAttrDescription];
        
       
        NSLog(@"username: %@", [keychain objectForKey:(__bridge id)kSecAttrAccount]);
        
        NSLog(@"password: %@",[keychain objectForKey:(__bridge id)kSecValueData]);
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
