//
//  SettingsTableViewController.m
//  DPOS Driver
//
//  Created by Simon Canil on 4/04/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import "SettingsTableViewController.h"
#import "SWRevealViewController.h"

@interface SettingsTableViewController ()

@end

@implementation SettingsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = @"Settings";
    
    //menu listener
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.menuButton setTarget: self.revealViewController];
        [self.menuButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    
    [self.menuButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                             [UIFont fontWithName:@"fontawesome" size:22.0], NSFontAttributeName,
                                             [UIColor whiteColor], NSForegroundColorAttributeName,
                                             nil]
                                   forState:UIControlStateNormal];
    self.menuButton.title  = @"\uf0c9";
    
    
     self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self setSwitchStates];

}

-(void) setSwitchStates{
    
    // get and set user defaults:
    NSString *autoCompleteOrder = [[NSUserDefaults standardUserDefaults]
                                   stringForKey:@"autoCompleteOrders"];
    NSString *autoCompleteRun = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"autoCompleteRun"];
    
    
    if ([autoCompleteOrder isEqualToString:@"YES"]){
        
        
        [self.autoCompleteOrderSwitch setOn:YES animated:YES];
        
           }
    else{
        
        [self.autoCompleteOrderSwitch setOn:NO animated:YES];

    }
    if ([autoCompleteRun isEqualToString:@"YES"]){
        
        [self.autoCompleteRunSwitch setOn:YES animated:YES];

    }else{
        
        [self.autoCompleteRunSwitch setOn:NO animated:YES];
    }
    
}



- (IBAction)autoCompleteOrder:(id)sender {
    
    if(self.autoCompleteOrderSwitch.on){
       
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"autoCompleteOrders"];

        
    }else{
        
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"autoCompleteOrders"];


    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)autoCompleteRun:(id)sender {
    
    if(self.autoCompleteRunSwitch.on){
        
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"autoCompleteRun"];
        
        
    }else{
        
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"autoCompleteRun"];
        
        
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source



/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
