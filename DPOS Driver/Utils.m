//
//  Utils.m
//  DPOS Driver
//
//  Created by Simon Canil on 10/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+ (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

+ (NSString *)stringFromTimeIntervalHourMinutes:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    NSString *remainingTime = [[NSString alloc] init];
    
    if((long) hours > 0) {
        remainingTime = [NSString stringWithFormat:@"%li hr %li min", (long)hours, (long)minutes];
    } else {
        remainingTime = [NSString stringWithFormat:@"%li min", (long)minutes];
    }
    
    return remainingTime;
}

+ (NSString *)stringMinutesFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    
    
    NSInteger minutes = floor(ti/60);
    
    
    return [NSString stringWithFormat:@"%ld", minutes];
 }

+ (NSString *)formatDateStringToTimeString: (NSString *) timeString{
    
    NSString *dateString = timeString;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:dateString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
    
    NSLog(@"Time converted: %@", [formatter stringFromDate:dateFromString]);
    
    return [formatter stringFromDate:dateFromString];
    
     
}


@end
