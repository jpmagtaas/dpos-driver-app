//
//  OrderHolder.m
//  DPOS Driver
//
//  Created by Simon Canil on 8/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import "OrderHolder.h"

@implementation OrderHolder


#pragma mark -
#pragma mark NSCoding Protocol
- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:self.orders forKey:@"orders"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    
    if (self) {
        [self setOrders:[decoder decodeObjectForKey:@"orders"]];
    }
    
    return self;
}




@end
