//
//  Identity.m
//  DPOS Driver
//
//  Created by Simon Canil on 13/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import "Identity.h"

@implementation Identity

#pragma mark -
#pragma mark NSCoding Protocol
- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:self.responseCode forKey:@"responseCode"];
    [coder encodeObject:self.lat forKey:@"lat"];
    [coder encodeObject:self.lon forKey:@"lon"];
    [coder encodeObject:self.driverName forKey:@"driverName"];
    [coder encodeObject:self.storeName forKey:@"storeName"];
    [coder encodeObject:self.apiToken forKey:@"apiToken"];
    
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    
    if (self) {
        [self setResponseCode:[decoder decodeObjectForKey:@"responseCode"]];
        [self setLat:[decoder decodeObjectForKey:@"lat"]];
        [self setLon:[decoder decodeObjectForKey:@"lon"]];
        [self setDriverName:[decoder decodeObjectForKey:@"driverName"]];
        [self setStoreName:[decoder decodeObjectForKey:@"storeName"]];
        [self setApiToken:[decoder decodeObjectForKey:@"apiToken"]];
    }
    
    return self;
}

@end
