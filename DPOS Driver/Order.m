//
//  Order.m
//  DPOS Driver
//
//  Created by Simon Canil on 8/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import "Order.h"

@implementation Order

#pragma mark -
#pragma mark NSCoding Protocol
- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:self.orderId forKey:@"orderId"];
    [coder encodeObject:self.runId forKey:@"runId"];
    [coder encodeObject:self.streetName forKey:@"streetName"];
    [coder encodeObject:self.customerName forKey:@"customerName"];
    [coder encodeObject:self.suburb forKey:@"suburb"];
    [coder encodeObject:self.postCode forKey:@"postCode"];
    [coder encodeObject:self.inTime forKey:@"inTime"];
    [coder encodeObject:self.outTime forKey:@"outTime"];
    [coder encodeObject:self.outTime forKey:@"quotedTime"];
    [coder encodeObject:self.lat forKey:@"lat"];
    [coder encodeObject:self.lon forKey:@"lon"];
    [coder encodeObject:self.tenderAmount forKey:@"tenderAmount"];
    [coder encodeObject:self.driverRecievedtTime forKey:@"driverRecievedtTime"];
    [coder encodeObject:self.startTime forKey:@"startTime"];
    [coder encodeObject:self.endTime forKey:@"endTime"];
    [coder encodeObject:self.status forKey:@"status"];
    [coder encodeObject:self.orderNotes forKey:@"orderNotes"];
    [coder encodeObject:self.paidAmount forKey:@"paidAmount"];
    [coder encodeObject:self.customerPhone forKey:@"customerPhone"];
    [coder encodeObject:self.customerPhone forKey:@"responseCode"];
    
}


- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    
    if (self) {
        [self setOrderId:[decoder decodeObjectForKey:@"orderId"]];
        [self setRunId:[decoder decodeObjectForKey:@"runId"]];
        [self setStreetName:[decoder decodeObjectForKey:@"streetName"]];
        [self setCustomerName:[decoder decodeObjectForKey:@"customerName"]];
        [self setSuburb:[decoder decodeObjectForKey:@"suburb"]];
        [self setPostCode:[decoder decodeObjectForKey:@"postCode"]];
        [self setOutTime:[decoder decodeObjectForKey:@"outTime"]];
        [self setInTime:[decoder decodeObjectForKey:@"inTime"]];
        [self setQuotedTime:[decoder decodeObjectForKey:@"quotedTime"]];
        [self setLat:[decoder decodeObjectForKey:@"lat"]];
        [self setLon:[decoder decodeObjectForKey:@"lon"]];
        [self setTenderAmount:[decoder decodeObjectForKey:@"tenderAmount"]];
        [self setDriverRecievedtTime:[decoder decodeObjectForKey:@"driverRecievedtTime"]];
        [self setStartTime:[decoder decodeObjectForKey:@"startTime"]];
        [self setEndTime:[decoder decodeObjectForKey:@"endTime"]];
        [self setStatus:[decoder decodeObjectForKey:@"status"]];
        [self setOrderNotes:[decoder decodeObjectForKey:@"orderNotes"]];
        [self setPaidAmount:[decoder decodeObjectForKey:@"paidAmount"]];
        [self setCustomerPhone:[decoder decodeObjectForKey:@"customerPhone"]];
        [self setCustomerPhone:[decoder decodeObjectForKey:@"responseCode"]];
    }
    
    return self;
}

@end
