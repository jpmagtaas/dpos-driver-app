//
//  MapViewController.m
//  DPOS Driver
//
//  Created by Simon Canil on 8/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import "MapViewController.h"
#import "Annotation.h"
#import "Utils.h"
#import "Order.h"
#import "RestkitHelper.h"
#import "OrderWatcher.h"
#import "Identity.h"
#import "LocationShareModel.h"

#define THE_SPAN 0.03f;

@interface MapViewController ()
@property RestkitHelper *restkitHelper;

@property CLLocationCoordinate2D targetLocation;

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.locationManager = [[CLLocationManager alloc] init];
    
    [[self locationManager] setDelegate:self];
    
    //we have to setup the location maanager with permission in later iOS versions
   if ([[self locationManager] respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [[self locationManager] requestWhenInUseAuthorization];
    }
    
    _restkitHelper = [[RestkitHelper alloc] init];
    [_restkitHelper loadOrders];
    
    self.title = @"Map";
    
    [self annotateMap];
    [self plotNavigation];
    [[self locationManager] setDistanceFilter: 30];
    [[self locationManager] setDesiredAccuracy:kCLLocationAccuracyBest];
    [[self locationManager] startUpdatingLocation];
    [[self locationManager] setAllowsBackgroundLocationUpdates:YES];
    [[self locationManager] setPausesLocationUpdatesAutomatically:NO];
    
    //buttons
    
    
    
    if (@available(iOS 13.0, *)) {
        //UIBarButtonItem *mapButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemClose target:self action:@selector(dismiss)];
        
        UIBarButtonItem *mapButton = [[UIBarButtonItem alloc] initWithTitle:@"\uf057" style:UIBarButtonItemStylePlain target:self action:@selector(dismiss)];
        
        [mapButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
        [UIFont fontWithName:@"fontawesome" size:20.0], NSFontAttributeName,
        [UIColor whiteColor], NSForegroundColorAttributeName,
        nil]
                              forState:UIControlStateNormal];
        [mapButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
        [UIFont fontWithName:@"fontawesome" size:20.0], NSFontAttributeName,
        [UIColor grayColor], NSForegroundColorAttributeName,
        nil]
                              forState:UIControlStateHighlighted];
        
        self.navigationItem.rightBarButtonItem = mapButton;
    } else {
        // Fallback on earlier versions
        UIBarButtonItem *mapButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismiss)];
        
        self.navigationItem.rightBarButtonItem = mapButton;
    }
    
    //UIBarButtonItem *returnBaseButton = [[UIBarButtonItem alloc] initWithTitle:@"Return to Base" style:UIBarButtonItemStylePlain target:self action:@selector(returnToBase)];
    //self.navigationItem.leftBarButtonItem = returnBaseButton;
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(returnToBase) name:@"returnToBase" object:nil];
    
    if(self.isReturningBase) {
        [self returnToBase];
    }
       
}

- (IBAction)openMap:(id)sender {
    // Create an MKMapItem to pass to the Maps app
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:self.targetLocation
                                            addressDictionary:nil];
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];

    // Set the directions mode to "Walking"
    // Can use MKLaunchOptionsDirectionsModeDriving instead
    NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};
    // Get the "Current User Location" MKMapItem
    MKMapItem *currentLocationMapItem = [MKMapItem mapItemForCurrentLocation];
    // Pass the current location and destination map items to the Maps app
    // Set the direction mode in the launchOptions dictionary
    [MKMapItem openMapsWithItems:@[currentLocationMapItem, mapItem]
                    launchOptions:launchOptions];
}

-(void)dismiss{
    
    
    NSLog(@"Dismissing");
   [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
  
    
    
}



-(void) returnToBase {
    [self.mapVIew removeOverlays:self.mapVIew.overlays];
    
    [self.mapVIew setShowsUserLocation:YES];
    self.mapVIew.showsUserLocation = YES;
    
    //get home store marker
    _restkitHelper = [[RestkitHelper alloc] init];
    [_restkitHelper loadAuth];
    
    Identity *homeStoreLocation = _restkitHelper.identities[0];
    
    [self configureFooterHome:homeStoreLocation];
    
    NSLog(@"plotting home store lat: %@ lon: %@", homeStoreLocation.lat, homeStoreLocation.lon);
    
    NSMutableArray *allocations=[[NSMutableArray alloc]init];
    CLLocationCoordinate2D annotationCoord;
    
    Annotation *annotationPoint = [[Annotation alloc] init];
    annotationCoord.latitude = [homeStoreLocation.lat doubleValue ];
    annotationCoord.longitude = [homeStoreLocation.lon doubleValue ];
    
    annotationPoint.coordinate = annotationCoord;
    annotationPoint.title = homeStoreLocation.storeName;
    //annotationPoint.subtitle = homeStoreLocation.storeName;
    
    [allocations addObject:annotationPoint];
    
    //TODO Centre over current location.
    
    [self.mapVIew setUserTrackingMode:MKUserTrackingModeFollow];
    
    
    [self.mapVIew addAnnotations:allocations];
    
    
    self.title = homeStoreLocation.storeName;
    
    //set location to navigate to
    CLLocationCoordinate2D location;
    location.latitude = [homeStoreLocation.lat doubleValue];
    location.longitude =[homeStoreLocation.lon doubleValue];
    
    self.targetLocation = location;
    
    [[LocationShareModel sharedModel] setTargetLocation:self.targetLocation];
    
    [self updateMapStatus:self.targetLocation];
    
}

//Add function to redraw map every update on location passing the location
-(void)updateMapStatus: (CLLocationCoordinate2D) target {
    [self.mapVIew removeOverlays:self.mapVIew.overlays];
    
    [self.mapVIew setShowsUserLocation:YES];
    self.mapVIew.showsUserLocation = YES;
    
    //create placemark
    MKPlacemark* placemark = [[MKPlacemark alloc] initWithCoordinate:target addressDictionary:nil];
    MKMapItem* destinationMapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    
    //construct direction request
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    [request setSource:[MKMapItem mapItemForCurrentLocation]];
    [request setDestination:destinationMapItem];
    [request setTransportType:MKDirectionsTransportTypeAutomobile]; // This can be limited to automobile and walking directions.
    [request setRequestsAlternateRoutes:NO]; // Gives you several route options. Switched OFF - first route item chosen.
    
    //send request.
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
    
    [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        if (!error) {
            MKRoute *route1 = [[response routes] objectAtIndex:0];
            
            NSString *eta = [Utils stringFromTimeInterval:route1.expectedTravelTime];
            
            NSLog(@"ETA route1: %@", eta);
            
            self.etaLabel.text = [Utils stringFromTimeIntervalHourMinutes:route1.expectedTravelTime];
            
            NSString *distance = [[NSString alloc] init];
            
            if(route1.distance > 0) {
                distance = [NSString stringWithFormat:@"%.02f", route1.distance / 1000];
            }
            
            self.quotedLabel.text = [NSString stringWithFormat:@"%@ km", distance];
            
            for (MKRoute *route in [response routes]) {
                [self.mapVIew addOverlay:[route polyline] level:MKOverlayLevelAboveRoads]; // Draws the route above roads, but below labels.
                
                
                for (MKRouteStep *step in route.steps)
                {
                    
                    NSLog(@"Do this Next %@", step.instructions);
                }
                
            }
        }
    }];
}

-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *location = locations.lastObject;
    NSLog(@"map location hit %@", location);
    
    [self updateMapStatus:self.targetLocation];
    
}

// fucks the blue dot. first attempt at custom map icons and disclosure settings etc.
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
   
    if ([annotation isKindOfClass:[Annotation class]]){
    
    MKAnnotationView *annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"loc"];
    annotationView.canShowCallout = YES;
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    UIView *vw = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    vw.backgroundColor = [UIColor redColor];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 80, 30)];
    label.numberOfLines = 2;
    label.text = @"10m";
    [vw addSubview:label];
    annotationView.leftCalloutAccessoryView = vw;
    return annotationView;
        
    }else{
        
        return nil;
    }
}


-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    id <MKAnnotation> annotation = [view annotation];
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        NSLog(@"Clicked Pizza Shop");
    }
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Disclosure Pressed" message:@"Click Cancel to Go Back" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
   // [alertView show];
    
    CLLocationCoordinate2D coordinate = [annotation coordinate];
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate addressDictionary:nil];
    MKMapItem *mapitem = [[MKMapItem alloc] initWithPlacemark:placemark];
    mapitem.name = annotation.title;
    [mapitem openInMapsWithLaunchOptions:nil];
    
    
}


-(void) annotateMap{
    NSMutableArray *allocations=[[NSMutableArray alloc]init];
    CLLocationCoordinate2D annotationCoord;
    
    [self.mapVIew setShowsUserLocation:YES];
    
    self.mapVIew.showsUserLocation = YES;
    
    
    for (Order *order in _restkitHelper.orders) {
        NSLog(@"%@", order.customerName);
        NSLog(@"lat:%@", order.lat);
        NSLog(@"long:%@", order.lon);
        
        Annotation *annotationPoint = [[Annotation alloc] init];
        annotationCoord.latitude = [order.lat doubleValue ];
        annotationCoord.longitude = [order.lon doubleValue ];
        
        annotationPoint.coordinate = annotationCoord;
        annotationPoint.title = order.customerName;
        annotationPoint.subtitle = order.streetName;
        
        [allocations addObject:annotationPoint];

      
    }
    
    // add home store marker
    
    _restkitHelper = [[RestkitHelper alloc] init];
    
    [_restkitHelper loadAuth];
    
    Identity *identity = _restkitHelper.identities[0];
    
    NSLog(@"plotting home store lat: %@ lon: %@", identity.lat, identity.lon);
    
    
    
    //TODO Centre over current location.
    
    [self.mapVIew setUserTrackingMode:MKUserTrackingModeFollow];
    
    
    [self.mapVIew addAnnotations:allocations];
     
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
        [renderer setStrokeColor:[UIColor blueColor]];
        [renderer setLineWidth:5.0];
        return renderer;
    }
    return nil;
}


-(void) plotNavigation {
    
    //check OrderWatcher singleton to see if an existing order in progress.
    
    BOOL orderInProgress = [OrderWatcher sharedEngine].isOrderInProgress;
    
    if (orderInProgress){
        
        Order *currentOrder = [OrderWatcher sharedEngine].getCurrentOrder;
        NSLog(@"Order in progress found - commence config and navigation plot for %@", currentOrder.customerName);
        
        
        [self configureFooterInProgress: currentOrder];
        
        self.title = [@"#" stringByAppendingString:currentOrder.orderId];
        
        //set location to navigate to
        CLLocationCoordinate2D location;
        location.latitude = [currentOrder.lat doubleValue];
        location.longitude =[currentOrder.lon doubleValue];
        
        self.targetLocation = location;
        
        [[LocationShareModel sharedModel] setTargetLocation:self.targetLocation];
        
        //create placemark
        //MKPlacemark* placemark = [[MKPlacemark alloc] initWithCoordinate:location addressDictionary:nil];
        //MKMapItem* destinationMapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
        
        /** TODO ADD OPTION DETAILS */
        NSDictionary* mapLaunchOptions;
      //  [destinationMapItem openInMapsWithLaunchOptions:mapLaunchOptions];
        
//        //construct direction request
//        MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
//        [request setSource:[MKMapItem mapItemForCurrentLocation]];
//        [request setDestination:destinationMapItem];
//        [request setTransportType:MKDirectionsTransportTypeAutomobile]; // This can be limited to automobile and walking directions.
//        [request setRequestsAlternateRoutes:NO]; // Gives you several route options. Switched OFF - first route item chosen.
//
//        //send request.
//        MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
//
//        //for expected travel time only:
//        //  [directions calculateETAWithCompletionHandler:^(MKETAResponse *response, NSError *error)
//        //   {
//        //     NSString *eta = [Utils stringFromTimeInterval:response.expectedTravelTime];
//
//        //       NSLog(@"ETA route1: %@", eta);
//        //   }];
        
        //self.quotedLabel.text = [@"Quoted: " stringByAppendingString:[Utils formatDateStringToTimeString:currentOrder.quotedTime]];
        
        
//        [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
//            if (!error) {
//                MKRoute *route1 = [[response routes] objectAtIndex:0];
//
//                NSString *eta = [Utils stringFromTimeInterval:route1.expectedTravelTime];
//
//                NSLog(@"ETA route1: %@", eta);
//
//                self.etaLabel.text = [@"ETA: " stringByAppendingString:eta];
//
//
//
//                for (MKRoute *route in [response routes]) {
//                    [self.mapVIew addOverlay:[route polyline] level:MKOverlayLevelAboveRoads]; // Draws the route above roads, but below labels.
//
//
//                    for (MKRouteStep *step in route.steps)
//                    {
//
//                        NSLog(@"%@", step.instructions);
//                    }
//
//                }
//            }
//        }];
        
        [self updateMapStatus:self.targetLocation];
        
       
    }else{
        
        [self configureFooterStatic];
    }
    
        
}

-(void)configureFooterHome:(Identity *) identity {
    self.customerNameLabel.text = @"Navigating back to";
    
    self.addressLabel.hidden = NO;
    self.customerNameLabel.hidden = NO;
    self.suburbLabel.hidden = NO;
    self.etaHeaderLabel.hidden = NO;
    self.quotedHeaderLabel.hidden = NO;
    self.etaLabel.hidden = NO;
    self.quotedLabel.hidden = NO;
    self.addressLabel.text = identity.storeName;
}

-(void) configureFooterInProgress:(Order *) order{
    
    NSLog(@"configuring footer for order: %@", order.customerName);
    
    self.customerNameLabel.text = order.customerName;

    self.addressLabel.hidden = NO;
    self.customerNameLabel.hidden = NO;
    self.suburbLabel.hidden = NO;
    self.etaLabel.hidden = NO;
    self.quotedLabel.hidden = NO;
    self.addressLabel.text = order.streetName;
    self.suburbLabel.text = order.suburb;
    
    self.etaHeaderLabel.hidden = NO;
    self.quotedHeaderLabel.hidden = NO;
}

-(void) configureFooterStatic {
    
    self.customerNameLabel.text = @"No orders in progress.";
    self.addressLabel.hidden = YES;
    self.suburbLabel.hidden = YES;
    self.etaLabel.hidden = YES;
    self.quotedLabel.hidden = YES;
    
    self.etaHeaderLabel.hidden = YES;
    self.quotedHeaderLabel.hidden = YES;
    
    [UIView animateWithDuration:0.0
                     animations:^{
                         self.footHeight.constant = 50;
                         [self.view layoutIfNeeded];
                     }];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
