//
//  MenuViewController.m
//  DPOS Driver
//
//  Created by Simon Canil on 18/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import "MenuViewController.h"
#import "Identity.h"
#import "RestkitHelper.h"
#import "MenuTableViewCell.h"
#import "MapViewController.h"


@interface MenuViewController ()

@property RestkitHelper *restkitHelper;

@end

@implementation MenuViewController

NSString *driverName;
NSString *storeName;

NSArray *menuItems;
NSArray *menuDisplay;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    menuDisplay = @[@"first"];
    menuItems = @[ @"third", @"homeBase", @"fourth"];
    
    self.menuTable.backgroundColor = [UIColor clearColor];
    self.menuTable.opaque = NO;
    self.menuTable.backgroundView = nil;
    
    self.view.backgroundColor = [UIColor colorWithRed:45.0f/255.0f green:45.0f/255.0f blue:45.0f/255.0f alpha:1.0f];
    
    
    // user details
    
    [self loadAuth];
    
 
    
    
}


-(void)loadAuth{
    
    _restkitHelper = [[RestkitHelper alloc] init];
    
    [_restkitHelper loadAuth];
    
    Identity *id = _restkitHelper.identities[0];
    
    driverName = id.driverName;
    storeName = id.storeName;
    
    NSLog(@"*******************Driver name: %@", id.driverName);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0){
        
        return 1;
    }
    else{
        return 3;
    }
}





- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section

{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
   
    // provides controll on individual header sections for formatting.
    if (section == 0){
        [headerView setBackgroundColor:[UIColor colorWithRed:23.0f/255.0f green:23.0f/255.0f blue:23.0f/255.0f alpha:0.7f]];
        
        UILabel *label =   [[UILabel alloc] initWithFrame:CGRectMake(10, 3, tableView.bounds.size.width - 10, 18)];
        label.text = @"Account";
        label.textColor = [UIColor whiteColor];
        label.backgroundColor = [UIColor clearColor];
        [label setNumberOfLines:0];
        [label sizeToFit];
        [label setContentMode:UIViewContentModeCenter];
        [headerView addSubview:label];
        
    }
    else{
       [headerView setBackgroundColor:[UIColor colorWithRed:23.0f/255.0f green:23.0f/255.0f blue:23.0f/255.0f alpha:0.7f]];
        
        
        UILabel *label =   [[UILabel alloc] initWithFrame:CGRectMake(10, 3, tableView.bounds.size.width - 10, 18)];
        label.text = @"Menu";
        label.textColor = [UIColor whiteColor];
        label.backgroundColor = [UIColor clearColor];
        
        [label setNumberOfLines:0];
        [label sizeToFit];
        [label setContentMode:UIViewContentModeCenter];
        [headerView addSubview:label];
        
    }
    return headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self.menuTable deselectRowAtIndexPath:indexPath animated:YES];
    
    NSLog(@"selected row: %lu", indexPath.row);
    
    if (indexPath.row == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"toggleSideNav" object:nil userInfo:nil];
        
        
        UIStoryboard *story = [self storyboard];
        MapViewController *mapView = [story instantiateViewControllerWithIdentifier:@"mapNav"];
        UINavigationController *navControl = [[UINavigationController alloc] initWithRootViewController: mapView];
        navControl.modalPresentationStyle = UIModalPresentationPopover;
        navControl.modalTransitionStyle = UIModalPresentationPopover;
        
        [mapView setIsReturningBase:YES];
        
        [self presentViewController:navControl animated:YES completion:nil];
    }
    
    if (indexPath.row == 2){
        //logout
        [self loadAuth];
        [_restkitHelper clearAuth];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    
    //   UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Workflow" bundle:nil];
    //   UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"workflowListViewController"];
    
    //   [self.navigationController pushViewController:viewController animated:YES];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *CellIdentifier;
    if (indexPath.section == 0){
    CellIdentifier = [menuDisplay objectAtIndex:indexPath.row];
        
        
        
    }else{
        
        CellIdentifier = [menuItems objectAtIndex:indexPath.row];
    }
    
    
 
    
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.driverName.text = driverName;
    cell.storeName.text = storeName;

    
    //selection mask
    CAGradientLayer *selectedGrad = [CAGradientLayer layer];
    selectedGrad.frame = cell.bounds;
    
    // selectable for menu items only
    if (indexPath.section == 1){
    
    selectedGrad.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:25.0/255.0 green:25.0/255.0 blue:25.0/255.0 alpha:.50] CGColor], (id)[[UIColor colorWithRed:25.0/255.0 green:25.0/255.0 blue:25.0/255.0 alpha:.70] CGColor], nil];
    }
    [cell setSelectedBackgroundView:[[UIView alloc] init]];
    [cell.selectedBackgroundView.layer insertSublayer:selectedGrad atIndex:0];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        return 30;
    } else if(section == 1)
    {
        return 30;
    }
    return UITableViewAutomaticDimension;
}


//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ///   NSLog(@"Index Path: %@", indexPath);
    if (indexPath.section == 0)
        return 88;
    else
        return 44; //double size for all but the last row
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
