//
//  MenuTableViewCell.h
//  DPOS Driver
//
//  Created by Simon Canil on 19/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *logoutLabel;
@property (weak, nonatomic) IBOutlet UILabel *iconOrderFeed;
@property (weak, nonatomic) IBOutlet UILabel *iconDriver;
@property (weak, nonatomic) IBOutlet UILabel *driverName;
@property (weak, nonatomic) IBOutlet UILabel *iconStore;
@property (weak, nonatomic) IBOutlet UILabel *iconSettings;
@property (weak, nonatomic) IBOutlet UILabel *iconReturn;
@property (weak, nonatomic) IBOutlet UILabel *storeName;

@end
