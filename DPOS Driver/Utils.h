//
//  Utils.h
//  DPOS Driver
//
//  Created by Simon Canil on 10/02/2015.
//  Copyright (c) 2015 VisionApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+ (NSString *)stringFromTimeInterval:(NSTimeInterval)interval;

+ (NSString *)stringFromTimeIntervalHourMinutes:(NSTimeInterval)interval;

+ (NSString *)stringMinutesFromTimeInterval:(NSTimeInterval)interval;

+ (NSString *)formatDateStringToTimeString: (NSString *) timeString;

@end
